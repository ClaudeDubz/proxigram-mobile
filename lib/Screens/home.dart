import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Models/image.dart';
import 'package:proxigram/Screens/Fragments/notification_fragment.dart';
import '../Widgets/BottomNavigationMenuWidget/bottom_navigation_widget.dart';
import 'package:proxigram/Screens/Fragments/feed_fragment.dart';
import 'package:proxigram/Screens/Fragments/profile_fragment.dart';
import 'package:proxigram/Screens/Fragments/upload_fragment.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:proxigram/main.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'Fragments/search_fragment.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(HomeScreen());

class HomeScreen extends StatefulWidget {
  final List<String> fragmentItems = [
    "feed",
    "search",
    "upload",
    "stars",
    "profile"
  ];

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isScrolled = false;
  static bool isStoriesShown = false;
  static bool isHomeSelected = true;
  static bool isUploadSelected = false;
  static bool isSearchSelected = false;
  static bool isStarsSelected = false;
  static bool isProfileSelected = false;

  BottomNavWidget navWidget;

  int fragmentIndex;
  GlobalKey _bottomNavigationKey = GlobalKey();
  List<int> backStack = [];
  int tapCounter = 0;
  @override
  void initState() {
    super.initState();
    backStack.add(0);
    fragmentIndex = 0;
    _getFragmentIndex(0);
    isHomeSelected = true;
    isSearchSelected = false;
    isUploadSelected = false;
    isStarsSelected = false;
    isProfileSelected = false;
  }

  _getFragmentIndex(int pos) {
    switch (pos) {
      case 0:
        return FeedFragment();
      case 1:
        return SearchFragment();
      case 2:
        return UploadFragment();
      case 3:
        return NotificationFragment();
      case 4:
        return ProfileFragment();

      default:
        return FeedFragment();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          _onWillPop();
          return false;
        },
        child: MaterialApp(
            home: Scaffold(
          bottomNavigationBar: CurvedNavigationBar(
            key: _bottomNavigationKey,
            height: 50,
            animationDuration: Duration(milliseconds: 500),
            animationCurve: Curves.easeInOut,
            backgroundColor: Color(colorTertiary),
            color: Color(colorBackground),
            items: <Widget>[
              isHomeSelected
                  ? Container(
                      child: SvgPicture.asset(
                      "assets/icons/home_icon_solid.svg",
                      color: Color(colorSecondary),
                      fit: BoxFit.scaleDown,
                      height: 25,
                      width: 25,
                    ))
                  : Container(
                      padding: EdgeInsets.all(25),
                      child: SvgPicture.asset(
                        "assets/icons/home_icon_stroke.svg",
                        color: Color(colorText),
                        fit: BoxFit.fitHeight,
                      )),
              isSearchSelected
                  ? Container(
                      child: SvgPicture.asset(
                      "assets/icons/search_icon_solid.svg",
                      color: Color(colorSecondary),
                      fit: BoxFit.scaleDown,
                      height: 25,
                      width: 25,
                    ))
                  : Container(
                      padding: EdgeInsets.all(25),
                      child: SvgPicture.asset(
                        "assets/icons/search_icon_stroke.svg",
                        color: Color(colorText),
                        fit: BoxFit.fitHeight,
                      )),
              isUploadSelected
                  ? Container(
                      child: SvgPicture.asset(
                      "assets/icons/post_icon_solid.svg",
                      color: Color(colorSecondary),
                      fit: BoxFit.scaleDown,
                      height: 25,
                      width: 25,
                    ))
                  : Container(
                      padding: EdgeInsets.all(25),
                      child: SvgPicture.asset(
                        "assets/icons/post_icon_stroke.svg",
                        color: Color(colorText),
                        fit: BoxFit.fitHeight,
                      )),
              isStarsSelected
                  ? Container(
                      child: SvgPicture.asset(
                      "assets/icons/star_icon_solid.svg",
                      color: Color(colorSecondary),
                      fit: BoxFit.scaleDown,
                      height: 25,
                      width: 25,
                    ))
                  : Container(
                      padding: EdgeInsets.all(25),
                      child: SvgPicture.asset(
                        "assets/icons/star_icon_stroke.svg",
                        color: Color(colorText),
                        fit: BoxFit.fitHeight,
                      )),
              isProfileSelected
                  ? Container(
                      child: SvgPicture.asset(
                      "assets/icons/profile_icon_solid.svg",
                      color: Color(colorSecondary),
                      fit: BoxFit.scaleDown,
                      height: 25,
                      width: 25,
                    ))
                  : Container(
                      padding: EdgeInsets.all(25),
                      child: SvgPicture.asset(
                        "assets/icons/profile_icon_stroke.svg",
                        color: Color(colorText),
                        fit: BoxFit.fitHeight,
                      ))
            ],
            onTap: (index) {
              backStack.add(index);
              updateFragment(index);
            },
          ),
          resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            elevation: 2,
            backgroundColor: Color(colorBackground),
            centerTitle: false,
            title: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(80),
                child: Image.asset(
                  'assets/app/name.png',
                  fit: BoxFit.scaleDown,
                )),
            leading: Container(
                padding: EdgeInsets.only(top: 12, left: 12, bottom: 12),
                child: Image.asset(
                  "assets/app/app_icon.png",
                  fit: BoxFit.contain,
                )),
            actions: <Widget>[
              Container(
                  padding: EdgeInsets.all(10),
                  width: 50,
                  height: 50,
                  child: GestureDetector(
                    onTap: () {
                      Utils().logoutDialog(context);
                    },
                    child: SvgPicture.asset("assets/icons/logout.svg",
                        color: Color(colorPrimary)),
                  )),
            ],
          ),
          body: _getFragmentIndex(fragmentIndex),
        )));
  }

  updateFragment(int i) {
    print("BACKSTACK $backStack");
    switch (i) {
      case 0:
        {
          setState(() {
            isHomeSelected = !isHomeSelected;
            isProfileSelected = false;
            isSearchSelected = false;
            isStarsSelected = false;
            isUploadSelected = false;
            fragmentIndex = 0;
          });
          break;
        }
      case 1:
        {
          setState(() {
            isSearchSelected = !isSearchSelected;
            isHomeSelected = false;
            isProfileSelected = false;
            isStarsSelected = false;
            isUploadSelected = false;
            fragmentIndex = 1;
          });
          break;
        }
      case 2:
        {
          setState(() {
            isUploadSelected = !isUploadSelected;
            isHomeSelected = false;
            isSearchSelected = false;
            isStarsSelected = false;
            isProfileSelected = false;
            fragmentIndex = 2;
          });
          break;
        }
      case 3:
        {
          setState(() {
            isStarsSelected = !isStarsSelected;
            isHomeSelected = false;
            isSearchSelected = false;
            isProfileSelected = false;
            isUploadSelected = false;
            fragmentIndex = 3;
          });
          break;
        }
      case 4:
        {
          setState(() {
            isProfileSelected = !isProfileSelected;
            isHomeSelected = false;
            isSearchSelected = false;
            isStarsSelected = false;
            isUploadSelected = false;
            fragmentIndex = 4;
          });
          break;
        }

      default:
        return Text("Error");
    }
  }

  Future<bool> _onWillPop() {
    CurvedNavigationBarState navBarState = _bottomNavigationKey.currentState;

    if (backStack.length == 1) {
      if (tapCounter == 1) {
        SystemNavigator.pop();
      } else {
        setState(() {
          tapCounter++;
        });
        Fluttertoast.showToast(
            msg: "Tap again to Exit",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 1,
            backgroundColor: Colors.black38,
            textColor: Colors.white,
            fontSize: 15.0);
      }
    } else {
      setState(() {
        navBarState.setPage(backStack[backStack.length - 2]);
        backStack.removeLast();
        backStack.removeLast();
      });
    }
  }
}
