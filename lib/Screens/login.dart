import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:proxigram/Theme/colors.dart';
import '../main.dart';
import '../Widgets/LoginWidget/login_credits_widget.dart';
import '../Widgets/LoginWidget/login_form_widget.dart';
import '../Widgets/LoginWidget/login_social_widget.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key}) : super(key: key);

  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    Widget logoContainer = Container(
        margin: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 100,
              width: 100,
              child: Image.asset(
                'assets/app/app_icon.png',
                fit: BoxFit.contain,
              ),
            ),
            Container(
                width: 200,
                child: Image.asset('assets/app/name.png', fit: BoxFit.contain))
          ],
        ));

    return WillPopScope(
      onWillPop: () async {
        _onWillPop();
        return false;
      },
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: Container(
              color: Colors.white,
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topCenter,
                    child: logoContainer,
                  ),
                  // Column(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   mainAxisSize: MainAxisSize.min,
                  //   children: <Widget>[LoginFormWidget()],
                  // ),
                  Center(
                    child: LoginFormWidget(),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: CreditsWidget(),
                  ),

                  Positioned(
                    child: Text("v 4.2.3"),
                    top: 10,
                    right: 10,
                  )
                ],
              ))),
    );
  }

  Future<bool> _onWillPop() {
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => SystemNavigator.pop(),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        true;
  }
}
