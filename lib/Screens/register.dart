import 'package:flutter/material.dart';
import 'package:proxigram/Widgets/RegisterWidget/register_form.dart';
import 'package:proxigram/Theme/colors.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
          color: Color(colorBackground),
          child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: RegisterFormWidget())),
    );
  }
}
