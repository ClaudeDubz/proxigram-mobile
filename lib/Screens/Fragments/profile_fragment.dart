import 'dart:convert';
import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Widgets/ChewieItemWidget/video_item_widget.dart';
import 'package:proxigram/Widgets/EditProfileWidget/edit_profile_widget.dart';
import 'package:proxigram/Widgets/FullPostWidget/full_post_widget.dart';
import 'package:proxigram/Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_post_modal.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Models/V2/feed.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:http/http.dart' as http;
import 'package:video_player/video_player.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class ProfileFragment extends StatefulWidget {
  _ProfileFragmentState createState() => _ProfileFragmentState();
}

class _ProfileFragmentState extends State<ProfileFragment> {
  String imageType;
  static String defaultProfilePhoto = "";
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;
  String imageformat = '';
  List<Feed> feedList = [];
  String thumbnail = "";

  Future<List<Feed>> myPost;
  @override
  void initState() {
    defaultProfilePhoto = Constants.userFirstName[0].toUpperCase();

    super.initState();
    myPost = getMyPost();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<List<Feed>> getMyPost() async {
    var res = await http.get(
        Uri.encodeFull(APIServices.httpDomain + APIServices.postOwn),
        headers: {"Authorization": "Bearer " + Constants.token});

    if (res.statusCode == 200) {
      var data = json.decode(res.body);

      feedList = data.map<Feed>((json) => Feed.fromJson(json)).toList();
    }

    return feedList;
  }

  Future getVideoThumbnail(String videoPathUrl) async {
    thumbnail = await VideoThumbnail.thumbnailFile(
      video: videoPathUrl,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG, // the original resolution of the video
      quality: 75,
    );

    return File(thumbnail);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Color(colorBackground),
          child: Column(
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              width: 1,
                              color: Color(colorText).withOpacity(0.2)))),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.all(20),
                        height: 100,
                        width: 100,
                        child: GradientStroke(
                          strokeWidth: 2,
                          radius: 50,
                          gradient: LinearGradient(colors: [
                            Color(colorPrimary),
                            Color(colorSecondary)
                          ]),
                          child: Container(
                            height: 90,
                            width: 90,
                            child: ClipRRect(
                                borderRadius: new BorderRadius.circular(50),
                                child: Constants.userProfilePhoto == null
                                    ? Container(
                                        color: Color(colorPrimary),
                                        alignment: Alignment.center,
                                        child: Text(
                                          defaultProfilePhoto,
                                          style: TextStyle(
                                              color: Color(colorText),
                                              fontSize: 20),
                                        ),
                                      )
                                    : Container(
                                        color: Color(colorBackground),
                                        child: CachedNetworkImage(
                                          imageUrl:
                                              "${APIServices.httpDomain}${Constants.userProfilePhoto}",
                                          placeholder: (context, url) =>
                                              Image.asset(
                                                  'assets/app/app_icon.png'),
                                          errorWidget: (context, url, error) =>
                                              new Icon(Icons.error),
                                          fit: BoxFit.cover,
                                        ))),
                          ),
                          onPressed: () {
                            showProfileModal(
                              APIServices.httpDomain +
                                  Constants.userProfilePhoto,
                            );
                          },
                        ),
                      ),
                      Expanded(
                        child: Container(
                            child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            //PROFILE USER INFO
                            Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                  children: <Widget>[
                                    Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "${Constants.userFirstName} ${Constants.userLastName}",
                                          style: TextStyle(
                                              color: Color(colorText),
                                              fontSize: 25),
                                        )),
                                    Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          Constants.userName == null
                                              ? "${Constants.userFirstName}${Constants.userLastName}"
                                              : "@${Constants.userName}",
                                          style: TextStyle(
                                              color: Color(colorText),
                                              fontSize: 12),
                                          textAlign: TextAlign.center,
                                        )),
                                  ],
                                )),
                              ],
                            ),

                            //PROFILE SOCIAL STATUS
                            Row(
                              children: <Widget>[
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        child: Text("Followers",
                                            style: TextStyle(
                                                color: Color(colorText),
                                                fontWeight: FontWeight.w700)),
                                        margin: EdgeInsets.only(
                                            bottom: 5, top: 10)),
                                    Container(
                                      child: Text(
                                          Constants.numFollowers.toString()),
                                    )
                                  ],
                                )),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        child: Text("Following",
                                            style: TextStyle(
                                                color: Color(colorText),
                                                fontWeight: FontWeight.w700)),
                                        margin: EdgeInsets.only(
                                            bottom: 5, top: 10)),
                                    Container(
                                      child: Text(
                                          Constants.numFollowing.toString()),
                                    )
                                  ],
                                )),
                                Expanded(
                                    child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                        child: Text("Posts",
                                            style: TextStyle(
                                                color: Color(colorText),
                                                fontWeight: FontWeight.w700)),
                                        margin: EdgeInsets.only(
                                            bottom: 5, top: 10)),
                                    Container(
                                      child:
                                          Text(Constants.numPosts.toString()),
                                    )
                                  ],
                                )),
                              ],
                            )
                          ],
                        )),
                      )
                    ],
                  )),
              profileBioContainer(),
              followButtonWidget(),
              Expanded(
                  child: Container(
                      margin: EdgeInsets.only(top: 20),
                      child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                FutureBuilder(
                                    future: myPost,
                                    builder: (context, snapshot) {
                                      return snapshot.data != null
                                          ? myPostWidget(snapshot.data)
                                          : Center(
                                              child:
                                                  CircularProgressIndicator());
                                    })
                              ]))))
            ],
          )),
    );
  }

  Widget myPostWidget(List<Feed> feed) {
    return feed.isEmpty
        ? Center(
            child: Text("Upload a Gram!"),
          )
        : GridView.count(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            crossAxisCount: 3,
            children: List.generate(feedList.length, (index) {
              return feed[index].contents.length == 1
                  ? getImageType(feed[index].contents[0].content) == "image"
                      ? GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FullPostWidget(
                                    userImage: Constants.userProfilePhoto,
                                    firstName: Constants.userFirstName,
                                    lastName: Constants.userLastName,
                                    imageType: feed[index].contents[0].type,
                                    isPostLiked: feed[index].isLiked,
                                    postID: feed[index].id,
                                    likesCount: feed[index].likesCount,
                                    commentList: feed[index].comments,
                                    createdAt: feed[index].createdAt,
                                    caption: feed[index].caption,
                                    content: feed[index].contents,
                                  ),
                                ));
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              padding: EdgeInsets.all(2),
                              child: ClipRRect(
                                  borderRadius:
                                      new BorderRadius.all(Radius.circular(0)),
                                  child: CachedNetworkImage(
                                    imageUrl:
                                        "${APIServices.httpDomain}${feed[index].contents[0].content}",
                                    placeholder: (context, url) =>
                                        Image.asset('assets/app/app_icon.png'),
                                    errorWidget: (context, url, error) =>
                                        new Icon(Icons.error),
                                    fit: BoxFit.cover,
                                  ))))
                      : GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FullPostWidget(
                                    userImage: Constants.userProfilePhoto,
                                    firstName: Constants.userFirstName,
                                    lastName: Constants.userLastName,
                                    imageType: feed[index].contents[0].type,
                                    isPostLiked: feed[index].isLiked,
                                    postID: feed[index].id,
                                    likesCount: feed[index].likesCount,
                                    commentList: feed[index].comments,
                                    createdAt: feed[index].createdAt,
                                    caption: feed[index].caption,
                                    content: feed[index].contents,
                                  ),
                                ));
                          },
                          child: Stack(children: <Widget>[
                            Container(
                                padding: EdgeInsets.all(2),
                                child: ChewieItem(
                                  videoPlayerController:
                                      VideoPlayerController.network(
                                    "${APIServices.httpDomain}${feed[index].contents[0].content}",
                                  ),
                                  looping: true,
                                )),
                            Container(
                              margin: EdgeInsets.all(10),
                              alignment: Alignment.topRight,
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              child: Icon(
                                FontAwesomeIcons.video,
                                size: 20.0,
                                color: Color(colorRed),
                              ),
                            )
                          ]))
                  : GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => FullPostWidget(
                                userImage: Constants.userProfilePhoto,
                                firstName: Constants.userFirstName,
                                lastName: Constants.userLastName,
                                imageType: feed[index].contents[0].type,
                                isPostLiked: feed[index].isLiked,
                                postID: feed[index].id,
                                likesCount: feed[index].likesCount,
                                commentList: feed[index].comments,
                                createdAt: feed[index].createdAt,
                                caption: feed[index].caption,
                                content: feed[index].contents,
                              ),
                            ));
                      },
                      child: Stack(children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            padding: EdgeInsets.all(2),
                            child: ClipRRect(
                                borderRadius:
                                    new BorderRadius.all(Radius.circular(0)),
                                child: CachedNetworkImage(
                                  imageUrl:
                                      "${APIServices.httpDomain}${feed[index].contents[0].content}",
                                  placeholder: (context, url) =>
                                      Image.asset('assets/app/app_icon.png'),
                                  errorWidget: (context, url, error) =>
                                      new Icon(Icons.error),
                                  fit: BoxFit.cover,
                                ))),
                        Container(
                          margin: EdgeInsets.all(5),
                          alignment: Alignment.topRight,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Icon(
                            Icons.collections,
                            size: 30.0,
                            color: Color(colorRed),
                          ),
                        )
                      ]));
            }));
  }

  Widget profileBioContainer() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Center(
              child: Constants.userDescription == null
                  ? Text(
                      "Your Grammable Description",
                      style: TextStyle(color: Color(colorText), fontSize: 13),
                      textAlign: TextAlign.center,
                    )
                  : Text(
                      Constants.userDescription,
                      style: TextStyle(color: Color(colorText), fontSize: 13),
                      textAlign: TextAlign.center,
                    ))
        ],
      ),
    );
  }

  Widget followButtonWidget() {
    return Container(
      height: 30,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(bottom: 15),
            width: MediaQuery.of(context).size.width,
            child: Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            width: 1,
                            color: Color(colorText).withOpacity(0.2))))),
          ),
          Container(
              alignment: Alignment.center,
              child: RaisedButton(
                  color: Color(colorPrimary),
                  child: Text(
                    "edit profile",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    showEditProfileModal();
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0))))
        ],
      ),
    );
  }

  showProfileModal(String imageURL) {
    showModalBottomSheet(
      backgroundColor: Color(colorBackground),
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ViewPostModal(
            imageProvider: NetworkImage(imageURL),
          ),
        );
      },
    );
  }

  void showEditProfileModal() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EditProfileWidget(
            image: Constants.userProfilePhoto,
            firstName: Constants.userFirstName,
            lastName: Constants.userLastName,
            description: Constants.userDescription,
          ),
        ));
  }

  getImageType(String path) {
    if (path.toLowerCase().substring(path.length - 3) == "jpg" ||
        path.toLowerCase().substring(path.length - 3) == "peg" ||
        path.toLowerCase().substring(path.length - 3) == "gif" ||
        path.toLowerCase().substring(path.length - 3) == "png") {
      return "image";
    } else {
      return "video";
    }
  }
}
