import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_post_modal.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Models/V2/notification.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:http/http.dart' as http;
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:path_provider/path_provider.dart';

class NotificationFragment extends StatefulWidget {
  NotificationFragment({Key key}) : super(key: key);

  _NotificationFragmentState createState() => _NotificationFragmentState();
}

class _NotificationFragmentState extends State<NotificationFragment> {
  List<UserNotification> userNotificationList = [];

  VideoPlayerController videoPlayerController;
  ChewieController chewieController;
  String imageformat = '';
  String thumbnail = "";

  Future<List<UserNotification>> myNotifications;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    myNotifications = getNotifications();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Future<List<UserNotification>> getNotifications() async {
    var res = await http.get(
        Uri.encodeFull(APIServices.httpDomain + APIServices.notifications),
        headers: {"Authorization": "Bearer " + Constants.token});

    if (res.statusCode == 200) {
      var data = json.decode(res.body);

      userNotificationList = data
          .map<UserNotification>((json) => UserNotification.fromJson(json))
          .toList();
    }

    return userNotificationList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(colorBackground),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: FutureBuilder(
          future: myNotifications,
          builder: (context, snapshot) {
            return snapshot.data != null
                ? notificationWidget(snapshot.data)
                : Center(child: CircularProgressIndicator());
          },
        ));
  }

  Widget notificationWidget(List<UserNotification> notificationList) {
    return notificationList.isEmpty
        ? Center(
            child: Text("You don't have notifications"),
          )
        : ListView.builder(
            itemCount: notificationList.length,
            itemBuilder: (context, index) {
              return Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    child: getNotificationIcon(notificationList[index].action),
                  ),
                  Container(
                      width: 50,
                      height: 50,
                      child: GradientStroke(
                        strokeWidth: 2,
                        radius: 50,
                        gradient: LinearGradient(colors: [
                          Color(colorPrimary),
                          Color(colorSecondary)
                        ]),
                        child: Container(
                          height: 40,
                          width: 40,
                          child: ClipRRect(
                            borderRadius: new BorderRadius.circular(50),
                            child: notificationList[index].sender.image == null
                                ? Container(
                                    color: Color(colorPrimary),
                                    alignment: Alignment.center,
                                    child: Text(
                                      notificationList[index]
                                          .sender
                                          .fname[0]
                                          .toUpperCase(),
                                      style: TextStyle(
                                          color: Color(colorText),
                                          fontSize: 20),
                                    ),
                                  )
                                : CachedNetworkImage(
                                    imageUrl:
                                        "${APIServices.httpDomain}${notificationList[index].sender.image}",
                                    errorWidget: (context, url, error) =>
                                        new Icon(Icons.error),
                                    fit: BoxFit.cover,
                                  ),
                          ),
                        ),
                        onPressed: () {},
                      )),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        getNotificationText(
                            notificationList[index].action,
                            notificationList[index].sender.fname,
                            notificationList[index].sender.lname),
                        Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(top: 10, left: 10),
                            child: Utils().getNotificationDate(
                                notificationList[index].createdAt))
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.all(10),
                      width: 100,
                      height: 100,
                      child: notificationList[index].action == "follow"
                          ? Container()
                          : getImageType(notificationList[index]
                                      .post
                                      .contents[0]
                                      .content) ==
                                  "image"
                              ? GestureDetector(
                                  onTap: () {
                                    // showImageModal(
                                    //     APIServices.httpDomain +
                                    //         notificationList[index]
                                    //             .post
                                    //             .contents[0]
                                    //             .content,
                                    //     notificationList[index]
                                    //         .post
                                    //         .contents[0]
                                    //         .type,
                                    //     index);
                                  },
                                  child: Container(
                                      padding: EdgeInsets.all(2),
                                      child: ClipRRect(
                                        borderRadius: new BorderRadius.all(
                                            Radius.circular(5)),
                                        child: CachedNetworkImage(
                                          imageUrl:
                                              "${APIServices.httpDomain}${notificationList[index].post.contents[0].content}",
                                          errorWidget: (context, url, error) =>
                                              new Icon(Icons.error),
                                          fit: BoxFit.cover,
                                        ),
                                      )))
                              : GestureDetector(
                                  onTap: () {
                                    showImageModal(
                                        APIServices.httpDomain +
                                            notificationList[index]
                                                .post
                                                .contents[0]
                                                .content,
                                        notificationList[index]
                                            .post
                                            .contents[0]
                                            .type,
                                        index);
                                  },
                                  child: Stack(children: <Widget>[
                                    Container(
                                        color: Color(colorSecondary),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height:
                                            MediaQuery.of(context).size.height,
                                        child: FutureBuilder(
                                          future: getVideoThumbnail(
                                              "${APIServices.httpDomain}${notificationList[index].post.contents[0].content}"),
                                          builder: (context, snapshot) {
                                            return snapshot.data != null
                                                ? Image.file(
                                                    snapshot.data,
                                                    fit: BoxFit.cover,
                                                  )
                                                : Center(
                                                    child:
                                                        CircularProgressIndicator());
                                          },
                                        )),
                                    Container(
                                      alignment: Alignment.center,
                                      width: MediaQuery.of(context).size.width,
                                      height:
                                          MediaQuery.of(context).size.height,
                                      child: Icon(
                                        Icons.play_arrow,
                                        size: 75.0,
                                        color: Color(colorBackground),
                                      ),
                                    )
                                  ])))
                ],
              );
            },
          );
  }

  Future getVideoThumbnail(String videoPathUrl) async {
    thumbnail = await VideoThumbnail.thumbnailFile(
      video: videoPathUrl,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG, // the original resolution of the video
      quality: 75,
    );

    return File(thumbnail);
  }

  showImageModal(String imageURL, String imageType, int index) {
    if (imageType == "image") {
      showModalBottomSheet(
        backgroundColor: Color(colorBackground),
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: ViewPostModal(
              imageProvider: NetworkImage(imageURL),
            ),
          );
        },
      );
    } else {
      showModalBottomSheet(
        backgroundColor: Color(colorBackground),
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
              color: Colors.black,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 20, top: 30),
                    child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.angleLeft,
                              color: Color(colorBackground),
                            ),
                            Text(
                              "Back",
                              style: TextStyle(
                                color: Color(colorBackground),
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ))),
                Expanded(
                    child: imageType == "video"
                        ? Chewie(
                            controller: chewieController,
                          )
                        : Container())
              ]));
        },
      );
    }
  }

  getNotificationIcon(String value) {
    switch (value) {
      case "like":
        return Icon(
          Icons.star,
          color: Color(colorTertiary),
          size: 20,
        );

      case "comment":
        return Icon(
          FontAwesomeIcons.solidComments,
          color: Color(colorPrimary),
          size: 20,
        );

      case "share":
        return Icon(
          FontAwesomeIcons.share,
          color: Color(colorTertiary),
          size: 20,
        );

        break;
      default:
    }
  }

  getNotificationText(String value, String fname, String lname) {
    switch (value) {
      case "like":
        return Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Text(
              "$fname $lname STARRED your Gram",
              style: TextStyle(color: Color(colorSecondary), fontSize: 16),
            ));

      case "comment":
        return Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Text(
              "$fname $lname COMMENTED your Gram",
              style: TextStyle(color: Color(colorSecondary), fontSize: 16),
            ));

      case "share":
        return Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Text(
              "$fname $lname SHARED your Gram",
              style: TextStyle(color: Color(colorSecondary), fontSize: 16),
            ));

      case "follow":
        return Container(
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Text(
              "$fname $lname followed you",
              style: TextStyle(color: Color(colorSecondary), fontSize: 16),
            ));

        break;

      default:
    }
  }

  getImageType(String path) {
    if (path.toLowerCase().substring(path.length - 3) == "jpg" ||
        path.toLowerCase().substring(path.length - 3) == "peg" ||
        path.toLowerCase().substring(path.length - 3) == "gif" ||
        path.toLowerCase().substring(path.length - 3) == "png") {
      return "image";
    } else {
      return "video";
    }
  }
}
