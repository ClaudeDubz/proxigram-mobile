import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Widgets/ChewieItemWidget/video_item_widget.dart';
import 'package:proxigram/Widgets/FullPostWidget/full_post_widget.dart';
import 'package:proxigram/Widgets/SearchWidget/search_filter_widget.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Models/V2/feed.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:proxigram/Widgets/SearchWidget/search_widget.dart';
import 'package:video_player/video_player.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SearchFragment extends StatefulWidget {
  SearchFragment({Key key}) : super(key: key);

  _SearchFragmentState createState() => _SearchFragmentState();
}

List<StaggeredTile> generateRandomTile(int count) {
  return List.generate(count, (i) => StaggeredTile.count(1, i.isEven ? 2 : 1));
}

getTileValue(int i) {
  var counter1 = i + 8;
  bool isCounter1 = counter1.toString() == 'true';
  var counter2 = i + 18;
  bool isCounter2 = counter2.toString() == 'true';
  if (i == 1) {
    return StaggeredTile.count(2, 2);
  } else if (isCounter1) {
    return StaggeredTile.count(2, 2);
  } else if (isCounter2) {
    return StaggeredTile.count(2, 2);
  } else {
    return StaggeredTile.count(1, 1);
  }
}

class _SearchFragmentState extends State<SearchFragment> {
  var customTiles;
  List<Feed> feedList = [];
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;
  String imageformat = '';
  String thumbnail = "";

  Future<List<Feed>> searchFeed;

  Map<int, int> tileMap = {
    0: 1,
    1: 2,
    2: 1,
    3: 1,
    4: 1,
    5: 1,
    6: 1,
    7: 1,
    8: 1,
    9: 2,
    10: 1,
    11: 1,
    12: 1,
    13: 1,
    14: 1,
    15: 1,
    16: 1,
    17: 1
  };

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    searchFeed = getFeedAll();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<List<Feed>> getFeedAll() async {
    var res = await http.get(
        Uri.encodeFull(APIServices.httpDomain + APIServices.postGetAll),
        headers: {"Authorization": "Bearer " + Constants.token});

    if (res.statusCode == 200) {
      var data = json.decode(res.body);

      feedList = data.map<Feed>((json) => Feed.fromJson(json)).toList();
      customTiles = generateRandomTile(feedList.length);
    }

    return feedList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(colorBackground),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  border: Border(
                      bottom:
                          BorderSide(width: 1, color: Color(colorPrimary)))),
              padding: EdgeInsets.all(15),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SearchWidget(),
                      ));
                },
                child: Row(
                  children: <Widget>[
                    Icon(
                      FontAwesomeIcons.search,
                      size: 20,
                      color: Color(colorPrimary),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text(
                        "Search",
                        style: TextStyle(
                            fontSize: 20, color: Color(colorDisabled)),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: 50,
              child: SearchFilterWidget(),
            ),
            Expanded(
              child: FutureBuilder(
                future: searchFeed,
                builder: (context, snapshot) {
                  return snapshot.data != null
                      ? searchListWidget(snapshot.data)
                      : Center(child: CircularProgressIndicator());
                },
              ),
            )
          ],
        ));
  }

  Widget searchListWidget(List<Feed> feed) {
    return StaggeredGridView.countBuilder(
      crossAxisCount: 3,
      itemCount: feed.length,
      itemBuilder: (BuildContext context, int index) => new Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(0)),
              child: feed[index].contents.length == 1
                  ? getImageType(feed[index].contents[0].content) == "image"
                      ? GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FullPostWidget(
                                    userImage: Constants.userProfilePhoto,
                                    firstName: Constants.userFirstName,
                                    lastName: Constants.userLastName,
                                    imageType: feed[index].contents[0].type,
                                    isPostLiked: feed[index].isLiked,
                                    postID: feed[index].id,
                                    likesCount: feed[index].likesCount,
                                    commentList: feed[index].comments,
                                    createdAt: feed[index].createdAt,
                                    caption: feed[index].caption,
                                    content: feed[index].contents,
                                  ),
                                ));
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              padding: EdgeInsets.all(1),
                              child: ClipRRect(
                                  borderRadius:
                                      new BorderRadius.all(Radius.circular(0)),
                                  child: CachedNetworkImage(
                                    imageUrl:
                                        "${APIServices.httpDomain}${feed[index].contents[0].content}",
                                    placeholder: (context, url) =>
                                        Image.asset('assets/app/app_icon.png'),
                                    errorWidget: (context, url, error) =>
                                        new Icon(Icons.error),
                                    fit: BoxFit.cover,
                                  ))))
                      : GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FullPostWidget(
                                    userImage: Constants.userProfilePhoto,
                                    firstName: Constants.userFirstName,
                                    lastName: Constants.userLastName,
                                    imageType: feed[index].contents[0].type,
                                    isPostLiked: feed[index].isLiked,
                                    postID: feed[index].id,
                                    likesCount: feed[index].likesCount,
                                    commentList: feed[index].comments,
                                    createdAt: feed[index].createdAt,
                                    caption: feed[index].caption,
                                    content: feed[index].contents,
                                  ),
                                ));
                          },
                          child: Stack(children: <Widget>[
                            Container(
                                padding: EdgeInsets.all(1),
                                child: ChewieItem(
                                  videoPlayerController:
                                      VideoPlayerController.network(
                                    "${APIServices.httpDomain}${feed[index].contents[0].content}",
                                  ),
                                  looping: true,
                                )),
                            Container(
                              margin: EdgeInsets.all(10),
                              alignment: Alignment.topRight,
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              child: Icon(
                                FontAwesomeIcons.video,
                                size: 20.0,
                                color: Color(colorProxiWall),
                              ),
                            )
                          ]))
                  : GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => FullPostWidget(
                                userImage: Constants.userProfilePhoto,
                                firstName: Constants.userFirstName,
                                lastName: Constants.userLastName,
                                imageType: feed[index].contents[0].type,
                                isPostLiked: feed[index].isLiked,
                                postID: feed[index].id,
                                likesCount: feed[index].likesCount,
                                commentList: feed[index].comments,
                                createdAt: feed[index].createdAt,
                                caption: feed[index].caption,
                                content: feed[index].contents,
                              ),
                            ));
                      },
                      child: Stack(children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            padding: EdgeInsets.all(1),
                            child: ClipRRect(
                                borderRadius:
                                    new BorderRadius.all(Radius.circular(0)),
                                child: CachedNetworkImage(
                                  imageUrl:
                                      "${APIServices.httpDomain}${feed[index].contents[0].content}",
                                  placeholder: (context, url) =>
                                      Image.asset('assets/app/app_icon.png'),
                                  errorWidget: (context, url, error) =>
                                      new Icon(Icons.error),
                                  fit: BoxFit.cover,
                                ))),
                        Container(
                          margin: EdgeInsets.all(10),
                          alignment: Alignment.topRight,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Icon(
                            Icons.collections,
                            size: 20.0,
                            color: Color(colorProxiWall),
                          ),
                        )
                      ])))),
      staggeredTileBuilder: (int index) =>
          new StaggeredTile.count(tileMap[index % 18], tileMap[index % 18]),
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }

  StaggeredTile getCustomTiles(int index) => customTiles[index];

  // Future getVideoThumbnail(String videoPathUrl) async {
  //   thumbnail = await VideoThumbnail.thumbnailFile(
  //     video: videoPathUrl,
  //     thumbnailPath: (await getTemporaryDirectory()).path,
  //     imageFormat: ImageFormat.PNG, // the original resolution of the video
  //     quality: 75,
  //   );

  //   return File(thumbnail);
  // }

  getImageType(String path) {
    if (path.toLowerCase().substring(path.length - 3) == "jpg" ||
        path.toLowerCase().substring(path.length - 3) == "peg" ||
        path.toLowerCase().substring(path.length - 3) == "gif" ||
        path.toLowerCase().substring(path.length - 3) == "png") {
      return "image";
    } else {
      return "video";
    }
  }
}
