import 'package:flutter/material.dart';
import 'package:proxigram/Models/image.dart';
import 'package:proxigram/Widgets/UploadWidget/image_description_widget.dart';
import 'package:proxigram/Widgets/UploadWidget/image_filter_widget.dart';
import 'package:proxigram/Widgets/UploadWidget/image_upload_widget.dart';
import 'package:proxigram/Theme/colors.dart';

void main() => runApp(UploadFragment());

class UploadFragment extends StatefulWidget {
  @override
  _UploadFragmentState createState() => _UploadFragmentState();
}

class _UploadFragmentState extends State<UploadFragment> {
  bool isFormComplete = false;
  bool isImageUploaded = false;
  bool isCaptionEmpty = true;

  int formIndex;

  String uploadTitle = "";

  @override
  void initState() {
    super.initState();
    formIndex = 0;
    uploadTitle = "UPLOAD";

    UserImage.videoFile = null;
    UserImage.imageFile = null;
    UserImage.isUploadImage = true;
  }

  @override
  void dispose() {
    super.dispose();
  }

  getFormIndex(int value) {
    switch (value) {
      case (0):
        return ImageUploadWidget();

      //   break;
      // case (1):
      //   return ImageFilterWidget();

        break;
      case (1):
        return ImageDescriptionWidget();

        break;

      default:
        return ImageUploadWidget();

        break;
    }
  }

  getTitle(int value) {
    switch (value) {
      case (0):
        return "UPLOAD";

      //   break;
      // case (1):
      //   return "EDIT";

        break;
      case (1):
        return "DESCRIPTION";

        break;

      default:
        return "UPLOAD";

        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(colorBackground),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Container(
                      height: 50,
                      width: 50,
                      alignment: Alignment.center,
                      child: InkWell(
                          onTap: () {
                            formIndex == 0 ? null : updateWidget(-1);
                          },
                          child: Icon(
                            Icons.arrow_left,
                            size: 50,
                            color: formIndex == 0
                                ? Color(colorBackgroundDarker)
                                : Color(colorPrimary),
                          )),
                    ),
                    Expanded(
                      child: Text(
                        getTitle(formIndex),
                        style: TextStyle(
                          color: Color(colorText),
                          fontSize: 20,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      alignment: Alignment.center,
                      child: InkWell(
                          onTap: () {
                            formIndex == 1 ? null : updateWidget(1);
                          },
                          child: Icon(Icons.arrow_right,
                              size: 50,
                              color: formIndex == 1
                                  ? Color(colorBackgroundDarker)
                                  : Color(colorPrimary))),
                    )
                  ],
                ),
              ],
            ),
            Expanded(
              child: getFormIndex(formIndex),
            ),
          ],
        ));
  }

  updateWidget(int index) {
    setState(() {
      formIndex = formIndex + index;
    
    });
  }
}
