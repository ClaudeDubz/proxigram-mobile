import 'package:flutter/material.dart';

import '../../Widgets/StoriesWidget/stories_widget.dart';
import '../../Widgets/PostWidget/post_widget.dart';
import 'package:proxigram/Theme/colors.dart';

class FeedFragment extends StatelessWidget {
  const FeedFragment({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Expanded(
              child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        height: 180,
                        child: StoriesWidget(),
                      ),
                      Container(
                          child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          PostWidget(),
                        ],
                      )),
                    ],
                  )))
        ]));
  }
}
