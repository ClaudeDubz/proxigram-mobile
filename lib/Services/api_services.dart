import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Models/V2/user.dart';
import 'package:proxigram/Screens/home.dart';
import 'package:proxigram/Screens/login.dart';
import '../Global/utils.dart';

class APIServices {
  static final String httpDomain =
      'https://proxigram-api.sidewalkdeveloper.com/';
  static final String register = 'api/v1/auth/register';
  static final String login = 'api/v1/auth/login';

  static final String postGetAll = 'api/v1/posts/all';
  static final String postAddNew = 'api/v1/posts';
  static final String postLike = 'api/v1/posts/like/';
  static final String postAddComment = 'api/v1/comments/';
  static final String postOwn = 'api/v1/posts/own';
  static final String postOthers = 'api/v1/posts/users/';
  static final String postView = 'api/v1/posts/ ';
  static final String usersAll = 'api/v1/users/all';
  static final String viewComments = 'api/v1/comments/all';
  static final String editUser = 'api/v1/users/edit';
  static final String viewUser = 'api/v1/users/';
  static final String notifications = 'api/v1/users/all_notifications';
  static final String addStories = 'api/v1/stories/';
  static final String viewStories = 'api/v1/stories/per-user';
  static final String followUser = 'api/v1/followers/follow/';
  static final String viewHashtag = 'api/v1/posts/hashtags/view/';
  static final String followHashtag = '/api/v1/hashtags?hashtag_id=';
  static final String resetPassword = 'api/v1/auth/password/email';
  static final String loginSocial = 'api/v1/auth/socialLogin';

  //REGISTRATION
  static Future<void> registerUser(BuildContext context, String fname,
      String lname, String email, String password, String username) async {
    Utils().showProgressDialog(context, "Registering account...");
    final userData = {
      "fname": fname,
      "lname": lname,
      "email": email,
      "password": password,
      "username": username,
    };

    final response = await http.post(
      APIServices.httpDomain + APIServices.register,
      body: userData,
    );

    if (response.statusCode == 200) {
      Utils().hideProgressDialog(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginScreen()));
    } else if (response.statusCode == 400) {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(context, "Register", "The email is already in use.");
    } else {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(
          context, "Register", "It's not you, it's us! Please try again.");
    }

    return response;
  }

  //LOGIN
  static Future<void> userLogin(
      BuildContext context, String email, String password) async {
    Utils().showProgressDialog(context, "Logging In...");
    final userData = {
      "email": email,
      "password": password,
    };

    final response = await http.post(
      APIServices.httpDomain + APIServices.login,
      body: userData,
    );

    if (response.statusCode == 200) {
      var resbody = json.decode(response.body);
      Constants.token = resbody["token"]["access_token"];
      Constants.userFirstName = resbody["profile"]["fname"];
      Constants.userLastName = resbody["profile"]["lname"];
      Constants.userCoverPhoto = resbody["profile"]["cover_photo"];
      Constants.userProfilePhoto = resbody["profile"]["image"];
      Constants.userEmail = resbody["profile"]["email"];
      Constants.userID = resbody["profile"]["id"];
      Constants.userDescription = resbody["profile"]["description"];
      Constants.userName = resbody["profile"]["username"];
      Constants.numFollowers = resbody["profile"]["followers_count"];
      Constants.numFollowing = resbody["profile"]["following_count"];
      Constants.numPosts = resbody["profile"]["num_post"];
      Constants.isSocial = false;

      Constants.sharedPreferences.setBool("login", true);
      Constants.sharedPreferences.setString("token", Constants.token);
      Constants.sharedPreferences.setString("first", Constants.userFirstName);
      Constants.sharedPreferences.setString("last", Constants.userLastName);
      Constants.sharedPreferences.setString("pp", Constants.userProfilePhoto);
      Constants.sharedPreferences.setString("email", Constants.userEmail);
      Constants.sharedPreferences.setInt("id", Constants.userID);
      Constants.sharedPreferences
          .setString("description", Constants.userDescription);
      Constants.sharedPreferences.setString("username", Constants.userName);
      Constants.sharedPreferences.setInt("followers", Constants.numFollowers);
      Constants.sharedPreferences.setInt("following", Constants.numFollowing);
      Constants.sharedPreferences.setInt("posts", Constants.numPosts);
      Constants.sharedPreferences.setBool("isSocial", Constants.isSocial);

      Utils().hideProgressDialog(context);

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else if (response.statusCode == 401) {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(context, "Login", "Invalid username or password.");
    } else {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(
          context, "Login", "It's not you, it's us! Please try again.");
    }

    return response;
  }

  //LOGIN
  static Future<void> socialLogin(
      BuildContext context, String accessToken, String provider) async {
    Utils().showProgressDialog(context, "Logging In...");
    final userData = {
      "access_token": accessToken,
      "provider": provider,
    };

    final response = await http.post(
      APIServices.httpDomain + APIServices.loginSocial,
      body: userData,
    );

    print(response.statusCode);

    if (response.statusCode == 200) {
      var resbody = json.decode(response.body);
      Constants.token = resbody["token"]["access_token"];
      Constants.userFirstName = resbody["profile"]["fname"];
      Constants.userLastName = resbody["profile"]["lname"];
      Constants.userCoverPhoto = resbody["profile"]["cover_photo"];
      Constants.userProfilePhoto = resbody["profile"]["image"];
      Constants.userEmail = resbody["profile"]["email"];
      Constants.userID = resbody["profile"]["id"];
      Constants.userDescription = resbody["profile"]["description"];
      Constants.userName = resbody["profile"]["username"];
      Constants.numFollowers = resbody["profile"]["followers_count"];
      Constants.numFollowing = resbody["profile"]["following_count"];
      Constants.numPosts = resbody["profile"]["num_post"];
      Constants.isSocial = true;
      Constants.provider = provider.toLowerCase();

      Constants.sharedPreferences.setBool("login", true);
      Constants.sharedPreferences.setString("token", Constants.token);
      Constants.sharedPreferences.setString("first", Constants.userFirstName);
      Constants.sharedPreferences.setString("last", Constants.userLastName);
      Constants.sharedPreferences.setString("pp", Constants.userProfilePhoto);
      Constants.sharedPreferences.setString("email", Constants.userEmail);
      Constants.sharedPreferences.setInt("id", Constants.userID);
      Constants.sharedPreferences
          .setString("description", Constants.userDescription);
      Constants.sharedPreferences.setString("username", Constants.userName);
      Constants.sharedPreferences.setInt("followers", Constants.numFollowers);
      Constants.sharedPreferences.setInt("following", Constants.numFollowing);
      Constants.sharedPreferences.setInt("posts", Constants.numPosts);
      Constants.sharedPreferences.setBool("isSocial", Constants.isSocial);
      Constants.sharedPreferences.setString("provider", Constants.provider);

      Utils().hideProgressDialog(context);

      print(Constants.userFirstName);
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else if (response.statusCode == 401) {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(context, "Login", "Invalid username or password.");
    } else {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(
          context, "Login", "It's not you, it's us! Please try again.");
    }

    return response;
  }

  //LIKE POST
  static likePost(int id, BuildContext context) {
    http.get('${APIServices.httpDomain}${APIServices.postLike}$id',
        headers: {"Authorization": "Bearer " + Constants.token});
  }

  //ADD COMMENT
  static commentPost(BuildContext context, String id, String comment) {
    final userData = {
      "post_id": id,
      "comment": comment,
    };

    http.post(
      APIServices.httpDomain + APIServices.postAddComment,
      headers: {"Authorization": "Bearer " + Constants.token},
      body: userData,
    );
  }

  //ADD NEW POST
  static Future<void> addPost(
      BuildContext context, String caption, List<File> files) async {
    Utils().showProgressDialog(context, "Posting gram...");
    String apiUrl = '${APIServices.httpDomain}${APIServices.postAddNew}';
    var request = http.MultipartRequest("POST", Uri.parse(apiUrl));
    //add text fields
    request.fields["caption"] = caption;
    request.headers["Authorization"] = "Bearer ${Constants.token}";
    //create multipart using filepath, string or bytes
    for (int i = 0; i < files.length; i++) {
      var pic = await http.MultipartFile.fromPath("content[]", files[i].path);
      request.files.add(pic);
    }

    //add multipart to request

    var response = await request.send();

    //Get the response from the server
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    if (response.statusCode == 200) {
      Utils().hideProgressDialog(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else if (response.statusCode == 413) {
      Utils().hideProgressDialog(context);
      Utils().largeFileErrorDialog(context);
    } else {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(
          context, "GRAM", "It's not you, it's us! Please try again.");
    }
  }

  //ADD STORY
  static Future<void> addStory(BuildContext context, File image) async {
    String apiUrl = '${APIServices.httpDomain}${APIServices.addStories}';
    var request = http.MultipartRequest("POST", Uri.parse(apiUrl));
    request.headers["Authorization"] = "Bearer ${Constants.token}";
    //create multipart using filepath, string or bytes
    var pic = await http.MultipartFile.fromPath("content", image.path);
    //add multipart to request
    request.files.add(pic);
    var response = await request.send();

    if (response.statusCode == 200) {
      Utils().hideProgressDialog(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => HomeScreen()));
    } else if (response.statusCode == 413) {
      Utils().hideProgressDialog(context);
      Utils().largeFileErrorDialog(context);
    } else {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(
          context, "Story", "It's not you, it's us! Please try again.");
    }
  }

  //FOLLOW USER
  static userFollow(int userID) {
    http.post('${APIServices.httpDomain}${APIServices.followUser}$userID',
        headers: {"Authorization": "Bearer " + Constants.token});
  }

  //FOLLOW HASHTAG
  static hashtagFollow(int id) {
    http.post('${APIServices.httpDomain}${APIServices.followHashtag}$id',
        headers: {"Authorization": "Bearer " + Constants.token});
  }

  //RESET PASSWORD
  static Future<void> passwordReset(BuildContext context, String email) async {
    Utils().showProgressDialog(context, "Sending Reset Link...");
    String apiUrl = '${APIServices.httpDomain}${APIServices.resetPassword}';
    var request = http.MultipartRequest("POST", Uri.parse(apiUrl));
    request.fields["email"] = email;
    var response = await request.send();

    if (response.statusCode == 200) {
      Utils().hideProgressDialog(context);
      Utils().successDialog(
          context, "RESET LINK", "reset link sent to your email");
    } else if (response.statusCode == 406) {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(context, "RESET LINK", "Email doesn't exist");
    } else {
      Utils().hideProgressDialog(context);
      Utils().errorDialog(
          context, "Story", "It's not you, it's us! Please try again.");
    }
  }
}
