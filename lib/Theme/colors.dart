import 'package:flutter/material.dart';

const kprimaryColor = const Color(0xFF44C2F1);
const kaccentColor = const Color(0xFFff8b50);
const ktextColor = const Color(0xFF474747);

//V1
// const int colorPrimary = 0xFF27c6ff;
// const int colorSecondary = 0xFFeeb91c;
// const int colorTertiary = 0xFFff8b50;
// const int colorBackground = 0xFFfcf5ef;
// const int colorBackgroundDarker = 0xFF209ebb;
// const int colorText = 0xFF032539;
// const int colorStatus = 0xFFB0B0B5;
// const int colorFacebook = 0xFF3b5998;
// const int colorProxiWall = 0xFFf9f1f1;

// //V2
// const int colorPrimary = 0xFFd08829;
// const int colorSecondary = 0xFF2c3e4b;
// const int colorTertiary = 0xFFd1aa47;
// const int colorBackground = 0xFFfcf5ef;
// const int colorBackgroundDarker = 0xFF757071;
// const int colorText = 0xFF032539;
// const int colorStatus = 0xFFB0B0B5;
// const int colorFacebook = 0xFF3b5998;
// const int colorProxiWall = 0xFFf9f1f1;

//V3
// const int colorPrimary = 0xFF42c6e8;
// const int colorSecondary = 0xFF3092d0;
// const int colorTertiary = 0xFFffb701;
// const int colorBackground = 0xFFfcf5ef;
// const int colorBackgroundDarker = 0xFFadd9d8;
// const int colorText = 0xFF125488;
// const int colorStatus = 0xFFB0B0B5;
// const int colorFacebook = 0xFF3b5998;
// const int colorProxiWall = 0xFFf9f1f1;
// const int colorBlack = 0xFF212121;
// const int colorDisabled = 0xFFababab;

//PIXORAX
const int colorPrimary = 0xFFf75c30;
const int colorSecondary = 0xFFffb701;
const int colorTertiary = 0xFF0061bf;
const int colorBackground = 0xFFfcf5ef;
const int colorBackgroundDarker = 0xFFadd9d8;
const int colorText = 0xFF170f28;
const int colorStatus = 0xFFB0B0B5;
const int colorFacebook = 0xFF3b5998;
const int colorProxiWall = 0xFFfc1212;
const int colorRed = 0xFFff2449;
const int colorBlack = 0xFF212121;
const int colorDisabled = 0xFFababab;

