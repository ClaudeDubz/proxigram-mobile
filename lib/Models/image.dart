
import 'dart:io';
import 'package:image/image.dart' as imageLib;

class UserImage{
  static File imageFile;
  static File coverPhotoFile;
  static String imageTitle;
  static String imageCaption;
  static imageLib.Image imagtoFilter;
  static File videoFile;
  static bool isUploadImage;
  static List<File> fileList = List<File>();
}