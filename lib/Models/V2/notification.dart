import 'package:proxigram/Models/V2/feed.dart';
import 'package:proxigram/Models/V2/user.dart';

class UserNotification {
  // int id;
  // int senderUserID;
  // int notifiableID;
  // String action;
  // User sender;
  // Feed post;
  // DateTime createdAt;

  // UserNotification({
  //   this.id,
  //   this.senderUserID,
  //   this.notifiableID,
  //   this.action,
  //   this.sender,
  //   this.post,
  //   this.createdAt,
  // });

  // factory UserNotification.fromJson(Map<String, dynamic> json) {
  //   return UserNotification(
  //     id: json['id'] as int,
  //     senderUserID: json['sender_user_id'] as int,
  //     notifiableID: json['notifiable_id'] as int,
  //     action: json['action'] as String,
  //     sender: User.fromJson(json['sender']),
  //     post: Feed.fromJson(json['notifiable']),
  //     createdAt: DateTime.parse(json["created_at"]),
  //   );
  // }

  int id;
  int senderUserID;
  int notifiableID;
  String notifiableType;
  String message;
  String action;
  int senderUserId;
  DateTime createdAt;
  String imageAddress;
  User sender;
  Feed post;

  UserNotification({
    this.id,
    this.senderUserID,
    this.notifiableID,
    this.notifiableType,
    this.message,
    this.action,
    this.senderUserId,
    this.createdAt,
    this.imageAddress,
    this.sender,
    this.post,
  });

  UserNotification.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    senderUserID = json['user_id'];
    notifiableID = json['notifiable_id'];
    notifiableType = json['notifiable_type'];
    message = json['message'];
    action = json['action'];
    senderUserId = json['sender_user_id'];
    createdAt = DateTime.parse(json["created_at"]);
    imageAddress = json['image_address'];
    sender = json['sender'] != null ? new User.fromJson(json['sender']) : null;
    post = json['content'] != null
        ? new Feed.fromJson(json['content'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.senderUserID;
    data['notifiable_id'] = this.notifiableID;
    data['notifiable_type'] = this.notifiableType;
    data['message'] = this.message;
    data['action'] = this.action;
    data['sender_user_id'] = this.senderUserId;
    data['created_at'] = this.createdAt;
    data['image_address'] = this.imageAddress;

    if (this.sender != null) {
      data['sender'] = this.sender.toJson();
    }
    if (this.post != null) {
      data['content'] = this.post.toJson();
    }
    return data;
  }
}
