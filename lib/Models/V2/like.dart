import 'package:proxigram/Models/V2/user.dart';

// class Like {
//   int likeID;
//   int userID;
//   int likeableID;
//   String likeableType;
//   User user;
//   Like({
//     this.likeID,
//     this.userID,
//     this.likeableID,
//     this.likeableType,
//     this.user,
//   });

//   factory Like.fromJson(Map<String, dynamic> json) {
//     return Like(
//       likeID: json['id'],
//       userID: json['user_id'],
//       likeableID: json['likeable_id'],
//       likeableType: json['likeable_type'],
//       user: json['user'],
//     );
//   }
// }

class Like {
  int id;
  int userId;
  int likeableId;
  String likeableType;
  String createdAt;
  String updatedAt;
  String createdAtTimezone;
  String updatedAtTimezone;
  String imageAddress;
  User user;

  Like(
      {this.id,
      this.userId,
      this.likeableId,
      this.likeableType,
      this.createdAt,
      this.updatedAt,
      this.createdAtTimezone,
      this.updatedAtTimezone,
      this.imageAddress,
      this.user});

  Like.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    likeableId = json['likeable_id'];
    likeableType = json['likeable_type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    createdAtTimezone = json['created_at_timezone'];
    updatedAtTimezone = json['updated_at_timezone'];
    imageAddress = json['image_address'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['likeable_id'] = this.likeableId;
    data['likeable_type'] = this.likeableType;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['created_at_timezone'] = this.createdAtTimezone;
    data['updated_at_timezone'] = this.updatedAtTimezone;
    data['image_address'] = this.imageAddress;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
