class Country {
  int countryID;
  String countryName;
  Country({
    this.countryID,
    this.countryName,
  });

  factory Country.fromJson(Map<String, dynamic> json) {
    return Country(
      countryID: json['id'],
      countryName: json['country_name'],
    );
  }
}
