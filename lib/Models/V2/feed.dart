import 'package:proxigram/Models/V2/comment.dart';
import 'package:proxigram/Models/V2/content.dart';
import 'package:proxigram/Models/V2/like.dart';
import 'package:proxigram/Models/V2/user.dart';

// class Feed {
//   final int postID;
//   final int userID;
//   final String caption;
//   final String image;
//   final int likesCount;
//   final List<Like> likeList;
//   final List<Comment> commentList;
//   final User user;
//   final String imageType;
//   final DateTime createdAt;
//   int isPostLiked;

//   Feed(
//       {this.postID,
//       this.userID,
//       this.caption,
//       this.image,
//       this.likesCount,
//       this.likeList,
//       this.commentList,
//       this.user,
//       this.imageType,
//       this.createdAt,
//       this.isPostLiked});

//   factory Feed.fromJson(Map<String, dynamic> json) {
//     return Feed(
//         postID: json['id'] as int,
//         userID: json['user_id'] as int,
//         caption: json['caption'] as String,
//         image: json['image'] as String,
//         likesCount: json['likes_count'] as int,
//         likeList: json['likes'].cast<Like>(),
//         commentList: List<Comment>.from(json["comments"].map((x) => Comment.fromJson(x))),
//         user: User.fromJson(json["user"]),
//         imageType: json['type'] as String,
//         createdAt: DateTime.parse(json["created_at"]),
//         isPostLiked: json['is_liked'] as int);
//   }
// }

class Feed {
  int id;
  int userId;
  String caption;
  DateTime createdAt;

  int likesCount;
  int isLiked;
  List<Comment> comments;
  List<Like> likes;
  User user;
  List<Contents> contents;

  Feed(
      {this.id,
      this.userId,
      this.caption,
      this.createdAt,
      this.likesCount,
      this.isLiked,
      this.comments,
      this.likes,
      this.user,
      this.contents});

  Feed.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    caption = json['caption'] ?? "";
    createdAt =  DateTime.parse(json["created_at"]);
    likesCount = json['likes_count'];
    isLiked = json['is_liked'];
    if (json['comments'] != null) {
      comments = new List<Comment>();
      json['comments'].forEach((v) {
        comments.add(new Comment.fromJson(v));
      });
    }
    if (json['likes'] != null) {
      likes = new List<Like>();
      json['likes'].forEach((v) {
        likes.add(new Like.fromJson(v));
      });
    }
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    if (json['contents'] != null) {
      contents = new List<Contents>();
      json['contents'].forEach((v) {
        contents.add(new Contents.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['caption'] = this.caption;
    data['created_at'] = this.createdAt;
    data['likes_count'] = this.likesCount;
    data['is_liked'] = this.isLiked;
    if (this.comments != null) {
      data['comments'] = this.comments.map((v) => v.toJson()).toList();
    }
    if (this.likes != null) {
      data['likes'] = this.likes.map((v) => v.toJson()).toList();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (this.contents != null) {
      data['contents'] = this.contents.map((v) => v.toJson()).toList();
    }
    return data;
  }
}