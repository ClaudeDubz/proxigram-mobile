import 'package:flutter/foundation.dart';
import 'package:proxigram/Models/V2/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Constants {
  static int selectedIndex;
  static String token = "";
  static String userProfilePhoto = "";
  static String userCoverPhoto = "";
  static String userFirstName = "";
  static String userLastName = "";
  static String userEmail = "";
  static String userDescription = "";
  static String userName = "";
  static int userID;
  static bool isLoggedIn = false;
  static int numFollowing;
  static int numFollowers;
  static int numPosts;
  static User user;
  static bool isSocial = false;
  static String provider = "";

  static SharedPreferences sharedPreferences;

  updateIndex(int value) {

    selectedIndex = value;
  }

  int getIndex() {
    return selectedIndex;
  }
}
