import 'package:flutter/material.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Theme/colors.dart';

class BottomNavWidget extends StatefulWidget {

  _BottomNavWidgetState createState() => _BottomNavWidgetState();
}

class _BottomNavWidgetState extends State<BottomNavWidget> {
  static bool isHomeSelected = true;
  static bool isUploadSelected = false;
  static bool isSearchSelected = false;
  static bool isStarsSelected = false;
  static bool isProfileSelected = false;

  var constants = new Constants();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
                color: Colors.black, offset: Offset(0, 0.1), blurRadius: 1.0)
          ],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50), topRight: Radius.circular(50)),
          color: Color(colorBackgroundDarker)),
      child: Row(
        children: <Widget>[
          Flexible(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      isHomeSelected = !isHomeSelected;
                      isProfileSelected = false;
                      isSearchSelected = false;
                      isStarsSelected = false;
                      isUploadSelected = false;
                      constants.updateIndex(0);
                    });
                  },
                  child: Icon(
                    Icons.home,
                    size: 25,
                    color:
                        isHomeSelected ? Color(colorSecondary) : Colors.white12,
                  )),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      isSearchSelected = !isSearchSelected;
                      isHomeSelected = false;
                      isProfileSelected = false;
                      isStarsSelected = false;
                      isUploadSelected = false;
                       constants.updateIndex(1);
                    });
                  },
                  child: Icon(
                    Icons.search,
                    size: 25,
                    color:
                        isSearchSelected ? Color(colorPrimary) : Colors.white12,
                  )),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      isUploadSelected = !isUploadSelected;
                      isHomeSelected = false;
                      isSearchSelected = false;
                      isStarsSelected = false;
                      isProfileSelected = false;
                       constants.updateIndex(2);
                    });
                  },
                  child: Icon(
                    Icons.file_upload,
                    size: 25,
                    color:
                        isUploadSelected ? Color(colorPrimary) : Colors.white12,
                  )),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      isStarsSelected = !isStarsSelected;
                      isHomeSelected = false;
                      isSearchSelected = false;
                      isProfileSelected = false;
                      isUploadSelected = false;
                       constants.updateIndex(3);
                    });
                  },
                  child: Icon(
                    Icons.star,
                    size: 25,
                    color:
                        isStarsSelected ? Color(colorPrimary) : Colors.white12,
                  )),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: GestureDetector(
                  onTap: () {
                    setState(() {
                      isProfileSelected = !isProfileSelected;
                      isHomeSelected = false;
                      isSearchSelected = false;
                      isStarsSelected = false;
                      isUploadSelected = false;
                       constants.updateIndex(4);
                    });
                  },
                  child: Icon(
                    Icons.person,
                    size: 25,
                    color: isProfileSelected
                        ? Color(colorPrimary)
                        : Colors.white12,
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
