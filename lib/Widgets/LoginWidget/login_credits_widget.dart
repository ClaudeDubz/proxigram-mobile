import 'package:flutter/material.dart';

class CreditsWidget extends StatefulWidget {
  CreditsWidget({Key key}) : super(key: key);

  _CreditsWidgetState createState() => _CreditsWidgetState();
}

class _CreditsWidgetState extends State<CreditsWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
              
                height: 50,
                width: 100,
                child: Image.asset('assets/app/proofsys_logo.png', fit: BoxFit.contain),
              )),
          Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "Powered By Proofsys.io",
                style: TextStyle(
                    color: Theme.of(context).accentColor, fontSize: 15),
              )),
        ],
      ),
    );
  }
}
