import 'package:flutter/material.dart';
import 'package:proxigram/Screens/register.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:proxigram/Widgets/LoginWidget/Dialog/custom_dialog.dart';

class LoginFormWidget extends StatefulWidget {
  LoginFormWidget({Key key}) : super(key: key);

  _LoginFormWidgetState createState() => _LoginFormWidgetState();
}

class _LoginFormWidgetState extends State<LoginFormWidget> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool autoValidateForm = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: formWidget(),
    );
  }

  Widget formWidget() {
    return Form(
      key: formKey,
      autovalidate: autoValidateForm,
      child: Container(
        margin: EdgeInsets.only( left: 50, right: 50),
        padding: EdgeInsets.only(left: 30, right: 30, top: 30, bottom: 30),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(const Radius.circular(20.0)),
            boxShadow: [
              BoxShadow(
                  color: Colors.black12,
                  offset: Offset(0.0, 10.0),
                  blurRadius: 10.0)
            ]),
        child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
          Container(
            child: TextFormField(
              controller: emailController,
              validator: (val) {
                if (val.isEmpty) {
                  return "email/username cannot be empty";
                } else {
                  return null;
                }
              },
              decoration: new InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  hintText: "email or username",
                  fillColor: Colors.white,
                  border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(20.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      borderSide:
                          BorderSide(width: 1, color: Color(colorPrimary)))),
              keyboardType: TextInputType.emailAddress,
              style: new TextStyle(
                  fontFamily: "Poppins", fontSize: 15, color: Color(colorText)),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 10),
              child: TextFormField(
                controller: passwordController,
                validator: (val) {
                  if (val.isEmpty) {
                    return "password cannot be empty";
                  } else {
                    return null;
                  }
                },
                obscureText: true,
                decoration: new InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    hintText: "password",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(20.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        borderSide:
                            BorderSide(width: 1, color: Color(colorPrimary)))),
                keyboardType: TextInputType.visiblePassword,
                style: new TextStyle(
                    fontFamily: "Poppins",
                    fontSize: 15,
                    color: Color(colorText)),
              )),
          Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.centerLeft,
              child: InkWell(
                child: Text(
                  "forgot password?",
                  textAlign: TextAlign.start,
                ),
                onTap: () {
                  openEmailDialog();
                },
              )),
          Container(
            width: double.maxFinite,
            child: RaisedButton(
                color: Color(colorPrimary),
                child: Text(
                  "LOGIN",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: loginAction,
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0))),
          ),
          Container(
            width: double.maxFinite,
            child: RaisedButton(
                color: Color(colorSecondary),
                child: Text(
                  "SIGNUP",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => RegisterScreen(),
                      ));
                },
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0))),
          ),
        ]),
      ),
    );
  }

  loginAction() {
    if (formKey.currentState.validate()) {
      APIServices.userLogin(
          context, emailController.text, passwordController.text);
    } else {
      setState(() {
        autoValidateForm = true;
      });
    }
  }

  openEmailDialog() {
    print("forgot");
    return showDialog(
      context: context,
      builder: (BuildContext context) => CustomDialog(),
    );
  }
}
