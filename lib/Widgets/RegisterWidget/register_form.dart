import 'package:flutter/material.dart';
import 'package:proxigram/Screens/login.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import '../../Services/api_services.dart';

class RegisterFormWidget extends StatefulWidget {
  @override
  _RegisterFormWidgetState createState() => _RegisterFormWidgetState();
}

class _RegisterFormWidgetState extends State<RegisterFormWidget> {
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final userNameController = TextEditingController();

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool autoValidateForm = false;
  bool isRegistered = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    firstNameController.dispose();
    lastNameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(20),
        child: Column(
        
      children: <Widget>[
        Center(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            width: 50,
            height: 50,
            child: Image.asset("assets/app/app_icon.png"),
          ),
        ),
        Center(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            child: Text(
              "REGISTER",
              style: TextStyle(
                  letterSpacing: 2,
                  fontSize: 50,
                  color: Color(colorText),
                  fontFamily: "Poppins"),
            ),
          ),
        ),
        Center(
          child: Container(
            margin: EdgeInsets.only(top: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginScreen(),
                    ));
              },
              child: Text(
                "I have an account",
                style: TextStyle(
                    fontSize: 15,
                    color: Color(colorSecondary),
                    fontFamily: "Poppins"),
              ),
            ),
          ),
        ),
        Form(
            key: formKey,
            autovalidate: autoValidateForm,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  //FIRST NAME
                  Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: TextFormField(
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Please provide first name";
                          } else {
                            return null;
                          }
                        },
                        controller: firstNameController,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            hintText: "First Name",
                            fillColor: Color(colorText),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    width: 1, color: Color(colorPrimary)))),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 15,
                            color: Color(colorText)),
                      )),

                  //LAST NAME
                  Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: TextFormField(
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Please provide last name";
                          } else {
                            return null;
                          }
                        },
                        controller: lastNameController,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            hintText: "Last Name",
                            fillColor: Color(colorText),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    width: 1, color: Color(colorPrimary)))),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 15,
                            color: Color(colorText)),
                      )),

                  //USER NAME
                  Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: TextFormField(
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Please provide username";
                          } else {
                            return null;
                          }
                        },
                        controller: userNameController,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            hintText: "Username",
                            fillColor: Color(colorText),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    width: 1, color: Color(colorPrimary)))),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 15,
                            color: Color(colorText)),
                      )),

                  //EMAIL
                  Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: TextFormField(
                        validator: (val) {
                          if (val.isEmpty) {
                            return "Please provide email";
                          } else {
                            if (validateEmail(val) != null) {
                              return "Please provide a valid email";
                            }
                          }
                        },
                        controller: emailController,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            hintText: "Email",
                            fillColor: Color(colorText),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    width: 1, color: Color(colorPrimary)))),
                        keyboardType: TextInputType.emailAddress,
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 15,
                            color: Color(colorText)),
                      )),

                  //PASSWORD
                  Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: TextFormField(
                        validator: (val) {
                          if (val.length < 6) {
                            return "Password length must not be less than 6";
                          } else {
                            return null;
                          }
                        },
                        controller: passwordController,
                        obscureText: true,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            hintText: "Password",
                            fillColor: Color(colorText),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    width: 1, color: Color(colorPrimary)))),
                        keyboardType: TextInputType.visiblePassword,
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 15,
                            color: Color(colorText)),
                      )),

                  //CONFIRM PASSWORD
                  Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: TextFormField(
                        validator: (val) {
                          if (val != passwordController.text) {
                            return "Password and Confirm Password must match";
                          } else {
                            return null;
                          }
                        },
                        obscureText: true,
                        decoration: new InputDecoration(
                            contentPadding: EdgeInsets.all(10),
                            hintText: "Confirm Password",
                            fillColor: Color(colorText),
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(20.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20)),
                                borderSide: BorderSide(
                                    width: 1, color: Color(colorPrimary)))),
                        keyboardType: TextInputType.visiblePassword,
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 15,
                            color: Color(colorText)),
                      )),

                  //REGISTER BUTTON
                  Center(
                      child: Container(
                    child: RaisedButton(
                      onPressed: validateForm,
                      color: Color(colorPrimary),
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      child: Text(
                        "SignUp",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ))
                ],
              ),
            )),
      ],
    ));
  }

  validateForm() {
    setState(() {
      isRegistered = true;
    });
    if (formKey.currentState.validate()) {
      APIServices.registerUser(
          context,
          firstNameController.text,
          lastNameController.text,
          emailController.text,
          passwordController.text,
          userNameController.text);
    } else {
      setState(() {
        autoValidateForm = true;
        isRegistered = false;
      });
    }
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Please enter a valid email';
    else
      return null;
  }
}
