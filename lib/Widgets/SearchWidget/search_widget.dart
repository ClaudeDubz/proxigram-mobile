import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Theme/colors.dart';

class SearchWidget extends StatefulWidget {
  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}

class _SearchWidgetState extends State<SearchWidget> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(120.0), // here the desired height
            child: AppBar(
              title: Container(
                margin: EdgeInsets.only(top: 30),
                child: TextField(
                  decoration: InputDecoration(
                      hintText: "Search",
                      hintStyle: TextStyle(color: Color(colorDisabled)),
                      contentPadding: EdgeInsets.all(5)),
                ),
              ),
              elevation: 1,
              leading: Container(
                margin: EdgeInsets.only(top: 20),
                child: IconButton(
                  icon: Icon(
                    FontAwesomeIcons.arrowAltCircleLeft,
                    size: 25,
                    color: Color(colorPrimary),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              backgroundColor: Colors.white,
              bottom: TabBar(
                indicatorWeight: 3,
                labelPadding: EdgeInsets.all(0),
                indicatorColor: Color(colorSecondary),
                labelColor: Color(colorSecondary),
                unselectedLabelColor: Color(colorDisabled),
                tabs: [
                  Tab(
                    text: "TOP",
                  ),
                  Tab(
                    text: "ACCOUNTS",
                  ),
                  Tab(
                    text: "TAGS",
                  ),
                  Tab(
                    text: "PLACES",
                  ),
                ],
              ),
            )),
        body: Stack(
          children: <Widget>[
            TabBarView(
              children: [
                Icon(Icons.directions_car),
                Icon(Icons.directions_transit),
                Icon(Icons.directions_bike),
                Icon(Icons.directions_bike),
              ],
            )
          ],
        ),
      ),
    );
  }
}
