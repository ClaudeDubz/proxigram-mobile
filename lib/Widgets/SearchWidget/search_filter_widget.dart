import 'package:flutter/material.dart';
import 'package:proxigram/Widgets/SearchWidget/chips_widget.dart';

class SearchFilterWidget extends StatefulWidget {
  @override
  _SearchFilterWidgetState createState() => _SearchFilterWidgetState();
}

class _SearchFilterWidgetState extends State<SearchFilterWidget> {
  List<SearchChipsWidget> chips = [
    SearchChipsWidget(
      chipName: "GTV",
    ),
    SearchChipsWidget(
      chipName: "Shop",
    ),
    SearchChipsWidget(
      chipName: "Music",
    ),
    SearchChipsWidget(
      chipName: "Movies",
    ),
    SearchChipsWidget(
      chipName: "Food",
    ),
    SearchChipsWidget(
      chipName: "Style",
    ),
    SearchChipsWidget(
      chipName: "Beauty",
    ),
    SearchChipsWidget(
      chipName: "Sports",
    ),
    SearchChipsWidget(
      chipName: "Art",
    ),
    SearchChipsWidget(
      chipName: "Nature",
    ),
    SearchChipsWidget(
      chipName: "Dance",
    ),
    SearchChipsWidget(
      chipName: "Travel",
    ),
    SearchChipsWidget(
      chipName: "Architecture",
    ),
    SearchChipsWidget(
      chipName: "Science & Tech",
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: chips.length,
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.only(left: 5, right: 5),
            child: chips[index],
          );
        },
      ),
    );
  }
}
