import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:proxigram/Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Models/V2/comment.dart';
import 'package:proxigram/Models/V2/user.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:http/http.dart' as http;

class ViewCommentModal extends StatefulWidget {
  final int postID;
  final List<Comment> comments;

  ViewCommentModal({@required this.postID, @required this.comments});

  _ViewCommentModalState createState() => _ViewCommentModalState();
}

class _ViewCommentModalState extends State<ViewCommentModal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20), topRight: Radius.circular(20))),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5),
            alignment: Alignment.center,
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        width: 1, color: Colors.white.withOpacity(0.2)))),
            child: Text(
              "Comments",
              style: TextStyle(color: Color(colorPrimary), fontSize: 18),
            ),
          ),
          Container(child: commentListWidget(widget.comments))
        ],
      ),
    );
  }

  Widget commentListWidget(List<Comment> comment) {
    return Expanded(
        child: ListView.builder(
      itemCount: comment.length,
      itemBuilder: (ctx, index) {
        return Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.all(10),
                  width: 60,
                  height: 60,
                  child: GradientStroke(
                    strokeWidth: 2,
                    radius: 50,
                    gradient: LinearGradient(
                        colors: [Color(colorPrimary), Color(colorSecondary)]),
                    child: Container(
                      height: 50,
                      width: 50,
                      child: ClipRRect(
                          borderRadius: new BorderRadius.circular(50),
                          child: comment[index].user.image == null
                              ? Container(
                                  color: Color(colorPrimary),
                                  alignment: Alignment.center,
                                  child: Text(
                                    comment[index].user.fname[0].toUpperCase(),
                                    style: TextStyle(
                                        color: Color(colorText), fontSize: 20),
                                  ),
                                )
                              : CachedNetworkImage(
                                  imageUrl:
                                      "${APIServices.httpDomain}${comment[index].user.image}",
                                  placeholder: (context, url) =>
                                      Image.asset('assets/app/app_icon.png'),
                                  errorWidget: (context, url, error) =>
                                      new Icon(Icons.error),
                                  fit: BoxFit.cover,
                                )),
                    ),
                    onPressed: () {},
                  )),
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    child: Text(
                      widget.comments[index].user.fname +
                          " " +
                          widget.comments[index].user.lname,
                      style:
                          TextStyle(color: Color(colorSecondary), fontSize: 15),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 5, right: 10),
                    child: Text(
                      widget.comments[index].comment,
                      style: TextStyle(color: Color(colorText), fontSize: 12),
                    ),
                  ),
                ],
              )),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Text(
                        "${comment[index].likesCount}",
                        style: TextStyle(
                            color: Color(colorTertiary), fontSize: 15),
                      ),
                    ),
                    Container(
                      child: GestureDetector(
                        child: Icon(
                          Icons.star,
                          size: 20,
                          color: Color(colorTertiary),
                        ),
                        onTap: () {},
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    ));
  }
}
