import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:proxigram/Models/V2/content.dart';
import 'package:proxigram/Widgets/ChewieItemWidget/feed_video_item_widget.dart';
import 'package:proxigram/Widgets/PostWidget/Animation/star_icon_animator.dart';
import 'package:proxigram/Widgets/PostWidget/Animation/star_overlay_animator.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_comment_modal.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_post_modal.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Models/V2/comment.dart';
import 'package:proxigram/Models/V2/feed.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_profile_modal.dart';
import 'package:proxigram/Widgets/PostWidget/post_description_widget.dart';
import '../../Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../Theme/colors.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';
import '../PostWidget/Modals/add_comment_modal.dart';
import '../../Models/V2/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:cached_network_image/cached_network_image.dart';
//import 'package:flutter_share_me/flutter_share_me.dart';

class PostWidget extends StatefulWidget {
  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> with TickerProviderStateMixin {
  AnimationController animationController;
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;

  bool isbuttonPressed = false;
  bool isLoopActive = false;

  int selectedPost;
  int counter = 0;
  int selectedIndex;

  String imageformat = '';
  String thumbnail;

  List<Feed> feedList = [];
  List<User> userList = [];
  List<List<Comment>> commentList = [];
  List<bool> isPostLikedList = [];
  List<int> postLikeCountList = [];
  List<int> postCommentCountList = [];

  final StreamController<void> _doubleTapImageEvents =
      StreamController.broadcast();

  Future<List<Feed>> getFeed;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat();

    getFeed = getFeedAll();
  }

  @override
  void dispose() {
    animationController.dispose();

    _doubleTapImageEvents.close();
    super.dispose();
  }

  void _onDoubleTapLikePhoto() {
    _doubleTapImageEvents.sink.add(null);

    setState(() {
      isPostLikedList[selectedIndex] = !isPostLikedList[selectedIndex];
      if (isPostLikedList[selectedIndex]) {
        setState(() {
          postLikeCountList[selectedIndex]++;
        });
      } else {
        setState(() {
          postLikeCountList[selectedIndex]--;
        });
      }
    });

    APIServices.likePost(feedList[selectedIndex].id, context);
  }

  void toggleLikePost() {
    setState(() {
      isPostLikedList[selectedIndex] = !isPostLikedList[selectedIndex];

      if (isPostLikedList[selectedIndex]) {
        setState(() {
          postLikeCountList[selectedIndex]++;
        });
      } else {
        setState(() {
          postLikeCountList[selectedIndex]--;
        });
      }
    });

    APIServices.likePost(feedList[selectedIndex].id, context);
  }

  Future getVideoThumbnail(String videoPathUrl) async {
    thumbnail = await VideoThumbnail.thumbnailFile(
      video: videoPathUrl,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG, // the original resolution of the video
      quality: 75,
    );

    return File(thumbnail);
  }

  Future<List<Feed>> getFeedAll() async {
    var res = await http.get(
        Uri.encodeFull(APIServices.httpDomain + APIServices.postGetAll),
        headers: {"Authorization": "Bearer " + Constants.token});

    if (res.statusCode == 200) {
      var data = json.decode(res.body);

      feedList = data.map<Feed>((json) => Feed.fromJson(json)).toList();
    }

    return feedList;
  }

  Widget feedListWidget(List<Feed> feed) {
    return feed.isEmpty
        ? Center(
            child: Text("Be the first to share a Gram!"),
          )
        : ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.vertical,
            itemCount: feed.length,
            itemBuilder: (ctx, index) {
              return Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      userInfo(
                        feed[index].user.id,
                        feed[index].user.image,
                        feed[index].user.fname,
                        feed[index].user.lname,
                        feed[index].user.description,
                        feed[index].user.username,
                        feed[index].user.followersCount,
                        feed[index].user.followingCount,
                        feed[index].user.numPost,
                        feed[index].user.isFollowed,
                      ),
                      userPost(feed[index].contents, index),
                      postIcons(
                          feed[index].isLiked,
                          feed[index].id,
                          feed[index].comments,
                          feed[index].createdAt,
                          index,
                          feed[index].contents[0].content),
                      postLikesCount(feed[index].likesCount, index),
                      postDescription(feed[index].caption,
                          feed[index].user.fname, feed[index].user.lname),
                      postComment(feed[index].comments, feed[index].id, index),
                      addComment(feed[index].id, index, feed[index].comments),
                    ],
                  ));
            },
          );
  }

  Widget addComment(int id, int index, List<Comment> comments) {
    return Container(
        margin: EdgeInsets.all(15),
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[
            Container(
                height: 25,
                width: 25,
                child: ClipRRect(
                    borderRadius: new BorderRadius.circular(50),
                    child: Constants.userProfilePhoto == null
                        ? Container(
                            color: Color(colorSecondary),
                            alignment: Alignment.center,
                            child: Text(
                              Constants.userFirstName[0].toUpperCase(),
                              style: TextStyle(
                                  color: Color(colorText), fontSize: 20),
                            ),
                          )
                        : CachedNetworkImage(
                            imageUrl:
                                "${APIServices.httpDomain}${Constants.userProfilePhoto}",
                            placeholder: (context, url) =>
                                Image.asset('assets/app/app_icon.png'),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                            fit: BoxFit.cover,
                          ))),
            Container(
                margin: EdgeInsets.only(left: 10),
                child: InkWell(
                    onTap: () => addCommentModal(id, index, comments),
                    child: Text(
                      "Add a comment ...",
                      style: TextStyle(color: Color(colorText)),
                    )))
          ],
        ));
  }

  Widget postComment(List<Comment> comments, int id, int index) {
    postCommentCountList.add(comments.length);

    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: 15, right: 15, top: 5),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
                onTap: () {},
                child: comments.length != 0
                    ? comments.length == 1
                        ? InkWell(
                            onTap: () {
                              showCommentsModal(id, comments);
                            },
                            child: Text(
                              "View ${postCommentCountList[index]} comment",
                              style: TextStyle(
                                  fontSize: 15, color: Color(colorText)),
                            ))
                        : InkWell(
                            onTap: () {
                              showCommentsModal(id, comments);
                            },
                            child: Text(
                              "View all ${postCommentCountList[index]} comments",
                              style: TextStyle(
                                  fontSize: 15, color: Color(colorText)),
                            ))
                    : Text(
                        "Be the first to comment!",
                        style: TextStyle(fontSize: 15, color: Color(colorText)),
                      )),
          ),
        ],
      ),
    );
  }

  Widget postDescription(String description, String fname, String lname) {
    return Container(
        margin: EdgeInsets.only(
          left: 15,
          top: 10,
        ),
        child: ExpandableDescription(
          description: description,
          user: "@$fname$lname",
        ));
  }

  Widget postLikesCount(int likesCount, int index) {
    postLikeCountList.add(likesCount);
    return Container(
      margin: EdgeInsets.only(left: 15),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: Text(
              postLikeCountList[index].toString(),
              style: TextStyle(
                  color: Color(colorText),
                  fontSize: 15,
                  fontWeight: FontWeight.w700),
            ),
          ),
          Container(
              child: Text(
            postLikeCountList[index] == 1 ? "Like" : "Likes",
            style: TextStyle(
                color: Color(colorText),
                fontSize: 15,
                fontWeight: FontWeight.w500),
          ))
        ],
      ),
    );
  }

  Widget userInfo(
      int id,
      String image,
      String firstName,
      String lastName,
      String description,
      String userName,
      int followers,
      int following,
      int posts,
      int isFollowed) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
              width: 60,
              height: 60,
              child: GradientStroke(
                strokeWidth: 2,
                radius: 50,
                gradient: LinearGradient(
                    colors: [Color(colorPrimary), Color(colorTertiary)]),
                child: Container(
                  height: 50,
                  width: 50,
                  child: ClipRRect(
                      borderRadius: new BorderRadius.circular(50),
                      child: image == null
                          ? Container(
                              color: Colors.white,
                              alignment: Alignment.center,
                              child: Text(
                                firstName[0].toUpperCase(),
                                style: TextStyle(
                                    color: Color(colorText), fontSize: 20),
                              ),
                            )
                          : CachedNetworkImage(
                              imageUrl: "${APIServices.httpDomain}$image",
                              placeholder: (context, url) =>
                                  Image.asset('assets/app/app_icon.png'),
                              errorWidget: (context, url, error) =>
                                  new Icon(Icons.error),
                              fit: BoxFit.cover,
                            )),
                ),
                onPressed: () {},
              )),
          Expanded(
              child: Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: InkWell(
                            onTap: () {
                              showUserProfile(
                                0,
                                image,
                                id,
                                firstName,
                                lastName,
                                description,
                                userName,
                                followers,
                                following,
                                posts,
                                isFollowed,
                              );
                            },
                            child: Text(
                              firstName + " " + lastName,
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Color(colorText),
                                  fontWeight: FontWeight.w500),
                            ),
                          )),
                    ],
                  ))),
        ],
      ),
    );
  }

  Widget userPost(List<Contents> content, int index) {
    return Stack(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(top: 10, bottom: 5),
            child: GestureDetector(
              onDoubleTap: () {
                selectedIndex = index;
                _onDoubleTapLikePhoto();
              },
              onLongPress: () {
                // showImageModal(
                //     "${APIServices.httpDomain}$image", "$imageType", index);
              },
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.width,
                  maxWidth: MediaQuery.of(context).size.width,
                  minWidth: MediaQuery.of(context).size.width,
                ),
                child: content.length == 1
                    ? getImageType(feedList[index].contents[0].content) ==
                            "image"
                        ? CachedNetworkImage(
                            imageUrl:
                                "${APIServices.httpDomain}${content[0].content}",
                            placeholder: (context, url) =>
                                Image.asset('assets/app/app_icon.png'),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                            fit: BoxFit.contain,
                          )
                        : ChewieFeedItem(
                            videoPlayerController:
                                VideoPlayerController.network(
                              "${APIServices.httpDomain}${content[0].content}",
                            ),
                            aspectRatio: VideoPlayerController.network(
                              "${APIServices.httpDomain}${content[0].content}",
                            ).value.aspectRatio,
                            looping: true,
                          )
                    : Swiper(
                        itemBuilder: (BuildContext context, int i) {
                          return Container(
                              padding: EdgeInsets.only(right: 5, left: 5),
                              child: getImageType(content[i].content) == "image"
                                  ? CachedNetworkImage(
                                      imageUrl:
                                          "${APIServices.httpDomain}${content[i].content}",
                                      placeholder: (context, url) =>
                                          Image.asset(
                                              'assets/app/app_icon.png'),
                                      errorWidget: (context, url, error) =>
                                          new Icon(Icons.error),
                                      fit: BoxFit.fitWidth,
                                    )
                                  : ChewieFeedItem(
                                      videoPlayerController:
                                          VideoPlayerController.network(
                                        "${APIServices.httpDomain}${content[i].content}",
                                      ),
                                      looping: true,
                                      aspectRatio:
                                          VideoPlayerController.network(
                                        "${APIServices.httpDomain}${content[i].content}",
                                      ).value.aspectRatio));
                        },
                        itemCount: content.length,
                        pagination: new SwiperPagination(
                            builder: DotSwiperPaginationBuilder(
                                color: Colors.white.withOpacity(0.75),
                                activeColor: Theme.of(context).primaryColor)),
                        loop: false,
                        duration: 100,
                      ),
              ),
            )),
        Positioned.fill(
          child: StarOverlayAnimator(
              triggerAnimationStream: _doubleTapImageEvents.stream),
        ),
      ],
    );
  }

  Widget postIcons(int isPostLiked, int postID, List<Comment> commentList,
      DateTime createdAt, int index, String imageUrl) {
    isPostLiked == 1 ? isPostLikedList.add(true) : isPostLikedList.add(false);

    return Container(
        margin: EdgeInsets.only(left: 10),
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(right: 15),
                alignment: Alignment.center,
                child: StarIconAnimator(
                  isLiked: isPostLikedList[index],
                  size: 30.0,
                  onTap: () {
                    selectedIndex = index;
                    toggleLikePost();
                  },
                  triggerAnimationStream: _doubleTapImageEvents.stream,
                )),
            Container(
              margin: EdgeInsets.only(right: 15),
              alignment: Alignment.center,
              child: GestureDetector(
                child: SvgPicture.asset(
                  "assets/icons/comment_icon_stroke.svg",
                  color: Color(colorDisabled),
                  height: 30,
                  width: 30,
                ),
                onTap: () {
                  showCommentsModal(postID, commentList);
                },
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: InkWell(
                child: SvgPicture.asset(
                  "assets/icons/share_icon_stroke.svg",
                  color: Color(colorDisabled),
                  height: 30,
                  width: 30,
                ),
                onTap: () async {
                  // var response = await FlutterShareMe()
                  //     .shareToSystem(msg: APIServices.httpDomain + imageUrl);
                  // if (response == 'success') {}
                  Fluttertoast.showToast(
                      msg: "Feature in progress ...",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIos: 1,
                      backgroundColor: Colors.black38,
                      textColor: Colors.white,
                      fontSize: 15.0);
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(top: 5, left: 10, right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[Utils().getNotificationDate(createdAt)],
                  )),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getFeed,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Container(
              margin: EdgeInsets.only(top: 15),
              child: feedListWidget(snapshot.data));
        } else {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Center(
              child: Container(
                width: 50,
                height: 50,
                child: CircularProgressIndicator(),
              ),
            ),
          );
        }
      },
    );
  }

  void addCommentModal(int i, int index, List<Comment> commentsList) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: AddCommentModal(
            onPost: (String text) {
              APIServices.commentPost(context, i.toString(), text);
              commentsList.add(Comment(
                  id: 1,
                  comment: text,
                  createdAt: DateTime.now().toString(),
                  deletedAt: "",
                  isLiked: 0,
                  likes: null,
                  likesCount: 0,
                  postId: 1,
                  updatedAt: "",
                  user: User(
                      image: Constants.userProfilePhoto,
                      fname: Constants.userFirstName,
                      lname: Constants.userLastName),
                  userId: Constants.userID));

              setState(() {
                postCommentCountList[index]++;
              });
              Navigator.pop(context);
            },
          ),
        );
      },
    );
  }

  void showImageModal(String imageURL, String imageType, int index) {
    if (imageType == "image") {
      showModalBottomSheet(
        backgroundColor: Color(colorBackground),
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: ViewPostModal(
              imageProvider: NetworkImage(imageURL),
            ),
          );
        },
      );
    } else {
      showModalBottomSheet(
        backgroundColor: Color(colorBackground),
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
              color: Colors.black,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 20, top: 30),
                    child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.angleLeft,
                              color: Color(colorBackground),
                            ),
                            Text(
                              "Back",
                              style: TextStyle(
                                color: Color(colorBackground),
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ))),
                Expanded(
                    child: imageType == "video"
                        ? Chewie(
                            controller: ChewieController(
                              videoPlayerController:
                                  VideoPlayerController.network(
                                      APIServices.httpDomain +
                                          feedList[index].contents[0].content),
                              aspectRatio: VideoPlayerController.network(
                                      APIServices.httpDomain +
                                          feedList[index].contents[0].content)
                                  .value
                                  .aspectRatio,
                              materialProgressColors: ChewieProgressColors(
                                playedColor: Color(colorSecondary),
                                handleColor: Color(colorPrimary),
                                bufferedColor: Color(colorPrimary),
                              ),
                              placeholder: Container(
                                color: Colors.grey,
                              ),
                              autoInitialize: true,
                              looping: false,
                              errorBuilder: (context, errorMessage) {
                                return Center(
                                  child: Text(
                                    errorMessage,
                                    style: TextStyle(color: Color(colorText)),
                                  ),
                                );
                              },
                            ),
                          )
                        : Container())
              ]));
        },
      );
    }
  }

  void showUserProfile(
    int userType,
    String image,
    int userID,
    String userFirstName,
    String userLastName,
    String userDescription,
    String userName,
    int numFollowers,
    int numFollowing,
    int numPost,
    int isFollowed,
  ) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      backgroundColor: Color(colorBackground),
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Color(colorBackground),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ViewProfileModal(
            userType: userType,
            image: image,
            userID: userID,
            userFirstName: userFirstName,
            userLastName: userLastName,
            userDescription: userDescription,
            userName: userName,
            numFollowers: numFollowers,
            numFollowing: numFollowing,
            numPost: numPost,
            isFollowed: isFollowed,
          ),
        );
      },
    );
  }

  void showCommentsModal(int id, List<Comment> comments) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      backgroundColor: Color(colorBackground),
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Color(colorBackground),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          height: MediaQuery.of(context).size.height / 1.5,
          width: MediaQuery.of(context).size.width,
          child: ViewCommentModal(
            postID: id,
            comments: comments,
          ),
        );
      },
    );
  }

  getImageType(String path) {
    if (path.toLowerCase().substring(path.length - 3) == "jpg" ||
        path.toLowerCase().substring(path.length - 3) == "peg" ||
        path.toLowerCase().substring(path.length - 3) == "gif" ||
        path.toLowerCase().substring(path.length - 3) == "png") {
      return "image";
    } else {
      return "video";
    }
  }
}
