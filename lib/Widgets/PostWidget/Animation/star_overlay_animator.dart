import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:proxigram/Theme/colors.dart';

class StarOverlayAnimator extends StatefulWidget {
  final Stream<void> triggerAnimationStream;

  StarOverlayAnimator({@required this.triggerAnimationStream});

  @override
  _StarOverlayAnimatorState createState() => _StarOverlayAnimatorState();
}

class _StarOverlayAnimatorState extends State<StarOverlayAnimator>
    with SingleTickerProviderStateMixin {
  AnimationController starController;
  Animation<double> starAnimation;

  @override
  void initState() {
    super.initState();
    final quick = const Duration(milliseconds: 400);
    final scaleTween = Tween(begin: 0.0, end: 1.0);
    starController = AnimationController(duration: quick, vsync: this);
    starAnimation = scaleTween.animate(
      CurvedAnimation(
        parent: starController,
        curve: Curves.elasticOut,
      ),
    );
    starController.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        starController.animateTo(
          0.0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.decelerate,
        );
      }
    });

    widget.triggerAnimationStream.listen((_) {
      starController
        ..reset()
        ..forward();
    });
  }

  @override
  void dispose() {
    starController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: ScaleTransition(
        scale: starAnimation,
        child: SvgPicture.asset(
          "assets/icons/star_icon_solid.svg",
          color: Colors.white60,
          height: 100,
          width: 100,
        ),
      ),
    );
  }
}
