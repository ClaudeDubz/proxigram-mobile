import 'package:flutter/material.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Theme/colors.dart';

class ExpandableDescription extends StatefulWidget {
  final String description;
  final String user;
  ExpandableDescription({@required this.description, @required this.user});

  @override
  _ExpandableDescriptionState createState() =>
      new _ExpandableDescriptionState();
}

class _ExpandableDescriptionState extends State<ExpandableDescription> {
  String firstHalf;
  String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.description.length > 50) {
      firstHalf = widget.description.substring(0, 30);
      secondHalf = widget.description.substring(30, widget.description.length);
    } else {
      firstHalf = widget.description;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: MediaQuery.of(context).size.width,
      child: secondHalf.isEmpty
          ? new Utils().convertHashtag(widget.description, widget.user, context)
          : new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                flag
                    ? RichText(
                        text: TextSpan(children: [
                        TextSpan(
                          text: widget.user + " ",
                          style: TextStyle(
                            color: Color(colorPrimary),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(
                            text: firstHalf + "...",
                            style: TextStyle(
                                color: Color(colorBlack), fontSize: 12))
                      ]))
                    : Utils().convertHashtag(
                        widget.description, widget.user, context),
                new InkWell(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      new Text(
                        flag ? "see more" : "see less",
                        style: new TextStyle(color: Colors.blue),
                      ),
                    ],
                  ),
                  onTap: () {
                    setState(() {
                      flag = !flag;
                    });
                  },
                ),
              ],
            ),
    );
  }
}
