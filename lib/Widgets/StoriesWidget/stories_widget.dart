import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Models/V2/stories.dart';
import 'package:proxigram/Models/V2/story.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:http/http.dart' as http;
import 'package:proxigram/Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'Modals/show_story_modal.dart';

class StoriesWidget extends StatefulWidget {
  StoriesWidget({Key key}) : super(key: key);

  _StoriesWidgetState createState() => _StoriesWidgetState();
}

class _StoriesWidgetState extends State<StoriesWidget> {
  List<Stories> storyList = [];
  bool isLoading = false;

  Future<List<Stories>> _getStories;
  int selectedIndex;
  int userIndex;
  @override
  void initState() {
    // TODO: implement initState
    isLoading = true;
    super.initState();
    _getStories = getStories();
  }

  Future<List<Stories>> getStories() async {
    var res = await http.get(
        Uri.encodeFull(APIServices.httpDomain + APIServices.viewStories),
        headers: {"Authorization": "Bearer " + Constants.token});

    if (res.statusCode == 200) {
      var data = json.decode(res.body);

      storyList = data.map<Stories>((json) => Stories.fromJson(json)).toList();
      isLoading = false;
      getStoryIndex(storyList);
    }

    return storyList;
  }

  getStoryIndex(List<Stories> stories) {
    for (int i = 0; i < stories.length; i++) {
      for (int j = 0; j < stories[i].story.length; j++) {
        if (stories[i].story[j].userId == Constants.userID) {
          userIndex = i;
        } else {}
      }
    }
  }

  Future uploadFromCamera() async {
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions(
            [PermissionGroup.camera, PermissionGroup.photos]);

    if (permissions[PermissionGroup.camera] == PermissionStatus.granted) {
      var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
      if (imageFile != null) {
        Utils().showProgressDialog(context, "Decoding Image...");
        compressImage(imageFile, imageFile.path);
      }
    } else {}
  }

  compressImage(File file, String targetPath) async {
    var compressedImage = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      targetPath,
      quality: 88,
    );

    if (userIndex != null) {
      storyList[userIndex].story.add(Story(
          content: compressedImage.path,
          createdAt: DateTime.now(),
          id: storyList[userIndex].story.length + 2,
          type: "image",
          userId: Constants.userID,
          recent: 0));
    } else {}

    APIServices.addStory(context, compressedImage);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: double.infinity,
        padding: EdgeInsets.only(top: 5, bottom: 5),
        child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
          storiesHeaderWidget(),
          Expanded(
            child: Container(
                padding: EdgeInsets.all(5),
                child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                  addStoryWidget(context),
                  Expanded(
                    child: FutureBuilder(
                      future: _getStories,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return storiesListWidget(snapshot.data);
                        } else {
                          return Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              child: Center(
                                child: Container(
                                  width: 50,
                                  height: 50,
                                  child: CircularProgressIndicator(),
                                ),
                              ));
                        }
                      },
                    ),
                  )
                ])),
          )
        ]));
  }

  Widget storiesHeaderWidget() {
    return Container(
        margin: EdgeInsets.only(right: 20),
        alignment: Alignment.centerRight,
        child: Text(
          "Recent Stories",
          style: TextStyle(
              fontSize: 15, fontFamily: 'Poppins', color: Color(colorText)),
        ));
  }

  Widget addStoryWidget(BuildContext context) {
    return GestureDetector(
      onTap: () => uploadFromCamera(),
      child: Container(
          width: 80,
          height: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                  child: Stack(children: <Widget>[
                Container(
                  height: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomLeft,
                        colors: [Color(colorPrimary), Color(colorSecondary)]),
                  ),
                  child: ClipRRect(
                    borderRadius: new BorderRadius.circular(8.0),
                    child: Constants.userProfilePhoto == null
                        ? Container(
                            color: Color(colorSecondary),
                            alignment: Alignment.center,
                            child: Text(
                              Constants.userFirstName[0].toUpperCase(),
                              style: TextStyle(
                                  color: Color(colorText), fontSize: 20),
                            ),
                          )
                        : CachedNetworkImage(
                            imageUrl:
                                "${APIServices.httpDomain}${Constants.userProfilePhoto}",
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
                Container(
                    padding: EdgeInsets.all(5),
                    alignment: Alignment.bottomRight,
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(35)),
                      ),
                      child: IconButton(
                        icon: Icon(Icons.add),
                        color: Color(colorSecondary),
                        onPressed: () => uploadFromCamera(),
                      ),
                    ))
              ])),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text("Add a Story"),
              ),
            ],
          )),
    );
  }

  Widget storiesListWidget(List<Stories> stories) {
    return stories.isEmpty
        ? Center(
            child: Text("Add a story"),
          )
        : Container(
            margin: EdgeInsets.only(left: 10),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: stories.length,
              itemBuilder: (context, index) {
                return Container(
                    margin: EdgeInsets.only(right: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Expanded(
                            child: GestureDetector(
                                onTap: () {
                                  showStoryModal(stories, index);
                                },
                                child: Stack(children: <Widget>[
                                  Container(
                                    width: 80,
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black,
                                            offset: Offset(0.1, 0.1),
                                            blurRadius: 0.1)
                                      ],
                                    ),
                                    child: ClipRRect(
                                      borderRadius:
                                          new BorderRadius.circular(8.0),
                                      child: CachedNetworkImage(
                                        imageUrl:
                                            "${APIServices.httpDomain}${stories[index].story[0].content}",
                                        errorWidget: (context, url, error) =>
                                            new Icon(Icons.error),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  Container(
                                      width: 80,
                                      padding: EdgeInsets.all(5),
                                      alignment: Alignment.bottomRight,
                                      child: Container(
                                          width: 45,
                                          height: 45,
                                          child: GradientStroke(
                                            strokeWidth: 2,
                                            radius: 50,
                                            gradient: LinearGradient(colors: [
                                              Color(colorPrimary),
                                              Color(colorSecondary)
                                            ]),
                                            child: Container(
                                                height: 40,
                                                width: 40,
                                                child: ClipRRect(
                                                    borderRadius:
                                                        new BorderRadius
                                                            .circular(50),
                                                    child: stories[index]
                                                                .image ==
                                                            null
                                                        ? Container(
                                                            color: Color(
                                                                colorPrimary),
                                                            alignment: Alignment
                                                                .center,
                                                            child: Text(
                                                              stories[index]
                                                                  .fname[0]
                                                                  .toUpperCase(),
                                                              style: TextStyle(
                                                                  color: Color(
                                                                      colorText),
                                                                  fontSize: 20),
                                                            ),
                                                          )
                                                        : CachedNetworkImage(
                                                            imageUrl:
                                                                "${APIServices.httpDomain}${stories[index].image}",
                                                            errorWidget: (context,
                                                                    url,
                                                                    error) =>
                                                                new Icon(Icons
                                                                    .error),
                                                            fit: BoxFit.cover,
                                                          ))),
                                            onPressed: () {},
                                          )))
                                ]))),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Text(stories[index].fname),
                        ),
                      ],
                    ));
              },
            ),
          );
  }

  void showStoryModal(List<Stories> stories, int _selectedIndex) {
    selectedIndex = _selectedIndex;
    showModalBottomSheet(
      backgroundColor: Color(colorBackground),
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: ViewStoryModal(
            storiesList: stories,
            inititalIndex: selectedIndex,
          ),
        );
      },
    );
  }
}
