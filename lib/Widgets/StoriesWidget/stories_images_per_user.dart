import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:countdown/countdown.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:photo_view/photo_view.dart';
import 'package:proxigram/Models/V2/stories.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:proxigram/Widgets/StoriesWidget/Modals/show_story_modal.dart';
import 'package:proxigram/Widgets/StoriesWidget/ProgressBar/progress_painter.dart';

class StoriesPerUser extends StatefulWidget {
  StoriesPerUser(
      {@required this.storiesList,
      @required this.selectedIndex,
      @required this.updateFunction});

  final List<Stories> storiesList;
  Function updateFunction;
  int selectedIndex;

  _StoriesPerUserState createState() => _StoriesPerUserState();
}

class _StoriesPerUserState extends State<StoriesPerUser> {
  int _index = 0;
  int storiesLength = 0;
  CountDown _countDown;
  StreamSubscription _countDownSubscription;
  int remainingTime = 5000;
  SwiperController swiperController;
  @override
  void initState() {
    super.initState();
    swiperController = SwiperController();
    this._initTimer();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _initTimer() {
    if (mounted)
      setState(() {
        this._countDown = CountDown(Duration(seconds: 5));
        this._countDown.remainingTime = Duration(milliseconds: remainingTime);
        this._countDownSubscription = this._countDown.stream.listen(null);
      });

    this._countDownSubscription.onData((d) {
      if (mounted) {
        setState(() {});
      } // Updates the UI to animate the progress bar
    });

    this._countDownSubscription.onDone(() {
      if (widget.storiesList[widget.selectedIndex].story.length ==
          this._index + 1) {
        if (widget.storiesList.length == widget.selectedIndex + 1) {
          Navigator.of(context).pop();
        } else {
          widget.updateFunction();
          setState(() {
            this._index = 0;
            this._initTimer();
          });
        }
      } else {
        if (mounted)
          setState(() {
            this._index++;
            this._countDownSubscription.cancel();
            this._initTimer();
          });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Swiper(
            controller: swiperController,
            onIndexChanged: (int i) {
              i = _index;
            },
            scrollDirection: Axis.horizontal,
            loop: false,
            autoplay: false,
            itemCount: widget.storiesList[widget.selectedIndex].story.length,
            itemBuilder: (context, index) {
              // return PhotoView(
              //   imageProvider: NetworkImage(APIServices.httpDomain +
              //       widget.storiesList[widget.selectedIndex].story[_index]
              //           .content),
              // );
              return widget.storiesList[widget.selectedIndex].story[_index]
                          .recent ==
                      1
                  ? CachedNetworkImage(
                      imageUrl:
                          "${APIServices.httpDomain}${widget.storiesList[widget.selectedIndex].story[_index].content}",
                      placeholder: (context, url) =>
                          Image.asset('assets/app/app_icon.png'),
                      errorWidget: (context, url, error) =>
                          new Icon(Icons.error),
                      fit: BoxFit.cover,
                    )
                  : Image.file(File(widget.storiesList[widget.selectedIndex]
                      .story[_index].content));
            },
          ),
          GestureDetector(
              onTap: () {
                setState(() {
                  updateIndex();
                });
              },
              onLongPress: () {
                this._countDownSubscription.pause();
              },
              onLongPressEnd: (details) {
                this._countDownSubscription.resume();
              },
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.transparent,
                  alignment: Alignment.topLeft,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            alignment: Alignment.topCenter,
                            width: MediaQuery.of(context).size.width,
                            height: 100,
                            child: Row(
                              children: <Widget>[
                                Container(
                                    margin: EdgeInsets.all(10),
                                    height: 40,
                                    width: 40,
                                    child: ClipRRect(
                                        borderRadius:
                                            new BorderRadius.circular(50),
                                        child: widget
                                                    .storiesList[
                                                        widget.selectedIndex]
                                                    .image ==
                                                null
                                            ? Container(
                                                color: Color(colorPrimary),
                                                alignment: Alignment.center,
                                                child: Text(
                                                  widget
                                                      .storiesList[
                                                          widget.selectedIndex]
                                                      .fname[0]
                                                      .toUpperCase(),
                                                  style: TextStyle(
                                                      color: Color(colorText),
                                                      fontSize: 20),
                                                ),
                                              )
                                            : CachedNetworkImage(
                                                imageUrl:
                                                    "${APIServices.httpDomain}${widget.storiesList[widget.selectedIndex].image}",
                                                placeholder: (context, url) =>
                                                    Image.asset(
                                                        'assets/app/app_icon.png'),
                                                errorWidget:
                                                    (context, url, error) =>
                                                        new Icon(Icons.error),
                                                fit: BoxFit.cover,
                                              ))),
                                Text(
                                  widget.storiesList[widget.selectedIndex]
                                          .fname +
                                      " " +
                                      widget.storiesList[widget.selectedIndex]
                                          .lname,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      Expanded(
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: List.generate(
                                widget.storiesList[widget.selectedIndex].story
                                    .length, (index) {
                              if (this._index == index) {
                                // Current active post
                                return Expanded(
                                  child: Container(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 5.0),
                                      child: CustomPaint(
                                        foregroundPainter: ProgressPainter(
                                            value: this
                                                    ._countDown
                                                    .remainingTime
                                                    .inMilliseconds /
                                                5000.0),
                                      )),
                                );
                              } else if (this._index > index) {
                                // when it's done
                                return Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: CustomPaint(
                                      foregroundPainter:
                                          ProgressPainter(value: 0.0),
                                    ),
                                  ),
                                );
                              } else {
                                // When it's next
                                return Expanded(
                                  child: Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: CustomPaint(
                                      foregroundPainter:
                                          ProgressPainter(value: 1.0),
                                    ),
                                  ),
                                );
                              }
                            }).reversed.toList(),
                          ),
                        ),
                      )
                    ],
                  )))
        ],
      ),
    );
  }

  updateIndex() {
    if (widget.storiesList[widget.selectedIndex].story.length ==
        this._index + 1) {
      setState(() {
        storiesLength++;
      });
      if (widget.storiesList.length == widget.selectedIndex + 1) {
        this._countDownSubscription.cancel();
        Navigator.of(context).pop();
      } else {
        widget.updateFunction();
        setState(() {
          this._index = 0;
          this._initTimer();
        });
      }
    } else {
      if (mounted)
        setState(() {
          this._index++;
          this._countDownSubscription.cancel();
          this._initTimer();
        });
    }
  }
}
