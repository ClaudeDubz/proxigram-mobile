import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Models/image.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:image/image.dart' as imageLib;
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:proxigram/Widgets/ChewieItemWidget/feed_video_item_widget.dart';
import 'package:thumbnails/thumbnails.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/services.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_native_image/flutter_native_image.dart';

class ImageUploadWidget extends StatefulWidget {
  ImageUploadWidget({Key key}) : super(key: key);

  _ImageUploadWidgetState createState() => _ImageUploadWidgetState();
}

class _ImageUploadWidgetState extends State<ImageUploadWidget> {
  imageLib.Image uploadedImage;
  String fileName;
  File image;
  String thumbnail;
  bool isFileImage;
  VideoPlayerController _controller;

  //MULTIPLE FILE
  String _fileName;
  String _path;
  Map<String, String> _paths;
  String _extension;
  bool _loadingPath = false;
  bool _multiPick = false;
  bool _hasValidMime = false;
  FileType _pickingType;
  TextEditingController textController = new TextEditingController();

  List<String> filePaths;
  int _counter = 1;

  @override
  void initState() {
    super.initState();

    textController.addListener(() => _extension = textController.text);
    image = UserImage.imageFile;
    isFileImage = UserImage.isUploadImage ?? true;
    if (!isFileImage) {
      if (UserImage.videoFile != null) {
        this.initChewie();
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  initChewie() {
    _controller = VideoPlayerController.file(UserImage.videoFile);
  }

  void openFiles() async {
    UserImage.fileList.clear();

    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions([
      PermissionGroup.camera,
      PermissionGroup.storage,
      PermissionGroup.photos
    ]);

    if (permissions[PermissionGroup.camera] == PermissionStatus.granted) {
      if (_pickingType != FileType.CUSTOM || _hasValidMime) {
        setState(() => _loadingPath = true);
        try {
          _path = null;
          _paths = await FilePicker.getMultiFilePath(
              type: _pickingType, fileExtension: _extension);
        } on PlatformException catch (e) {}
        if (!mounted) return;

        setState(() {
          _loadingPath = false;

          if (_paths != null) {
            _paths.forEach((fileName, filePath) => {compressFiles(filePath)});
          }
        });
      }
    } else {
      Utils().hideProgressDialog(context);
    }
  }

  void convertPathToFile(String path) {
    UserImage.fileList.add(File(path));
    compressFiles(path);
  }

  compressFiles(String path) async {
    File testFile = File(path);
    if (getImageType(path) == "image") {
      print("FILE SIZE BEFORE: " + testFile.lengthSync().toString());
      var compressedImage = await FlutterNativeImage.compressImage(
          File(path).path,
          quality: 50,
          percentage: 70);

      testFile = compressedImage;
      UserImage.fileList.add(testFile);
      setState(() {
        isFileImage = true;
        image = UserImage.fileList[0];
        UserImage.imagtoFilter = uploadedImage;
        UserImage.imageFile = image;
        UserImage.isUploadImage = true;
      });
      print("FILE SIZE  AFTER: " + testFile.lengthSync().toString());
    } else {
      UserImage.fileList.add(File(path));
      getVideoThumbnail(UserImage.fileList[0].path);
      setState(() {
        isFileImage = false;
        image = UserImage.fileList[0];
        UserImage.videoFile = image;
        UserImage.isUploadImage = false;
      });
    }

    print(UserImage.fileList);
  }

  Future openCamera() async {
    UserImage.fileList.clear();
    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions(
            [PermissionGroup.camera, PermissionGroup.photos]);

    if (permissions[PermissionGroup.camera] == PermissionStatus.granted) {
      var imageFile = await ImagePicker.pickImage(source: ImageSource.camera);

      if (imageFile != null) {
        Utils().showProgressDialog(context, "Decoding Image...");

        compressImage(imageFile, imageFile.path);
      }
    } else {
      Utils().hideProgressDialog(context);
    }
  }

  Future openVideo() async {
    UserImage.fileList.clear();

    Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler().requestPermissions(
            [PermissionGroup.storage, PermissionGroup.camera]);

    if (permissions[PermissionGroup.storage] == PermissionStatus.granted) {
      var videoFile = await ImagePicker.pickVideo(source: ImageSource.camera);
      if (videoFile != null) {
        UserImage.fileList.add(File(videoFile.path));
        getVideoThumbnail(UserImage.fileList[0].path);
        setState(() {
          isFileImage = false;
          image = UserImage.fileList[0];
          UserImage.videoFile = image;
          UserImage.isUploadImage = false;
        });
      }
    } else {}
  }

  getVideoThumbnail(String videoPathUrl) async {
    var thumbnail = await Thumbnails.getThumbnail(
      videoFile: videoPathUrl,
      imageType: ThumbFormat.JPEG,
      quality: 50,
    );

    setState(() {
      UserImage.imagtoFilter = uploadedImage;
      UserImage.imageFile = File(thumbnail);
    });
  }

  compressImage(File file, String targetPath) async {
    var compressedImage = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      targetPath,
      quality: 50,
    );

    setState(() {
      isFileImage = true;
      image = compressedImage;
      UserImage.imagtoFilter = uploadedImage;
      UserImage.imageFile = image;
      UserImage.isUploadImage = true;
    });
    UserImage.fileList.add(image);
    Utils().hideProgressDialog(context);
    print(" PATH" + file.absolute.path);
    print(" PATH" + targetPath);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  border: Border.all(
                color: Color(colorBackgroundDarker),
                width: 1.0,
              )),
              height: MediaQuery.of(context).size.width,
              width: MediaQuery.of(context).size.width,
              child: image == null
                  ? SvgPicture.asset(
                      "assets/icons/preview_icon.svg",
                      color: Color(colorBackgroundDarker),
                      height: 30,
                      width: 30,
                      fit: BoxFit.contain,
                    )
                  : UserImage.fileList.length == 1
                      ? isFileImage
                          ? Image.file(
                              image,
                              fit: BoxFit.scaleDown,
                            )
                          : ChewieFeedItem(
                              videoPlayerController: VideoPlayerController.file(
                                UserImage.videoFile,
                              ),
                              looping: true,
                              aspectRatio: VideoPlayerController.file(
                                      UserImage.videoFile)
                                  .value
                                  .aspectRatio)
                      : Swiper(
                          itemBuilder: (BuildContext context, int i) {
                            return Container(
                                padding: EdgeInsets.only(right: 5, left: 5),
                                child: ClipRRect(
                                    borderRadius: new BorderRadius.circular(0),
                                    child: getImageType(
                                                UserImage.fileList[i].path) ==
                                            "image"
                                        ? Image.file(
                                            UserImage.fileList[i],
                                            fit: BoxFit.scaleDown,
                                          )
                                        : ChewieFeedItem(
                                            videoPlayerController:
                                                VideoPlayerController.file(
                                              UserImage.fileList[i],
                                            ),
                                            looping: true,
                                            aspectRatio:
                                                VideoPlayerController.file(
                                              UserImage.fileList[i],
                                            ).value.aspectRatio)));
                          },
                          itemCount: UserImage.fileList.length,
                          pagination: new SwiperPagination(
                              builder: DotSwiperPaginationBuilder(
                                  color: Colors.white.withOpacity(0.75),
                                  activeColor: Theme.of(context).primaryColor)),
                          loop: false,
                          duration: 100,
                        )),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                  height: 50,
                  width: 50,
                  child: GestureDetector(
                    onTap: () => openFiles(),
                    child: SvgPicture.asset("assets/icons/gallery_icon.svg",
                        color: Color(colorBackgroundDarker)),
                  )),
              Container(
                  height: 50,
                  width: 50,
                  child: GestureDetector(
                    onTap: () {
                      openCamera();
                    },
                    child: SvgPicture.asset("assets/icons/camera_icon.svg",
                        color: Color(colorBackgroundDarker)),
                  )),
              Container(
                  height: 50,
                  width: 50,
                  child: GestureDetector(
                    onTap: () {
                      openVideo();
                    },
                    child: SvgPicture.asset("assets/icons/video_icon.svg",
                        color: Color(colorBackgroundDarker)),
                  )),
            ],
          ))
        ]);
  }

  getImageType(String path) {
    if (path.toLowerCase().substring(path.length - 3) == "jpg" ||
        path.toLowerCase().substring(path.length - 3) == "peg" ||
        path.toLowerCase().substring(path.length - 3) == "gif" ||
        path.toLowerCase().substring(path.length - 3) == "png") {
      return "image";
    } else {
      return "video";
    }
  }
}
