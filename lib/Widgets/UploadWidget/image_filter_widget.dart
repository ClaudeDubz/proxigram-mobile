import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:proxigram/Models/image.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:image_cropper/image_cropper.dart';

void main() => runApp(new MaterialApp(home: ImageFilterWidget()));

class ImageFilterWidget extends StatefulWidget {
  ImageFilterWidget({Key key}) : super(key: key);

  _ImageFilterWidgetState createState() => _ImageFilterWidgetState();
}

class _ImageFilterWidgetState extends State<ImageFilterWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(colorBackground),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      alignment: Alignment.center,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                border: Border.all(
              color: Color(colorBackgroundDarker),
              width: 1.0,
            )),
            height: MediaQuery.of(context).size.width,
            width: MediaQuery.of(context).size.width,
            child: UserImage.imageFile == null
                ? SvgPicture.asset("assets/icons/preview_icon.svg",
                    color: Color(colorBackgroundDarker))
                : Image.file(
                    UserImage.imageFile,
                    fit: BoxFit.scaleDown,
                  ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            alignment: Alignment.center,
            child: InkWell(
              child: SvgPicture.asset("assets/icons/edit_image_icon.svg",
                  color: Color(colorBackgroundDarker)),
              onTap: () {
               // imageCropper(UserImage.imageFile);
              },
            ),
          )
        ],
      ),
    );
  }

  // Future<Null> imageCropper(File imageFile) async {
  //   File croppedImage = await ImageCropper.cropImage(
  //       statusBarColor: Color(colorBackground),
  //       circleShape: false,
  //       sourcePath: imageFile.path,
  //       toolbarColor: Color(colorBackground),
  //       toolbarWidgetColor: Color(colorPrimary),
  //       toolbarTitle: "Edit Grammable");

  //   setState(() {
  //     UserImage.imageFile = croppedImage ?? UserImage.imageFile;
  //   });
  // }
}
