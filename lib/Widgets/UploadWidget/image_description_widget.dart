import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import '../../Models/image.dart';

class ImageDescriptionWidget extends StatefulWidget {
  ImageDescriptionWidget({Key key}) : super(key: key);

  _ImageDescriptionWidgetState createState() => _ImageDescriptionWidgetState();
}

class _ImageDescriptionWidgetState extends State<ImageDescriptionWidget> {
  final captionController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(colorBackground),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                    border: Border.all(
                  color: Color(colorBackgroundDarker),
                  width: 1.0,
                )),
                height: 100,
                width: 100,
                child: UserImage.imageFile == null
                    ? SvgPicture.asset("assets/icons/preview_icon.svg",
                        color: Color(colorBackgroundDarker))
                    : Image.file(
                        UserImage.imageFile,
                        fit: BoxFit.scaleDown,
                      ),
              ),
              Expanded(
                child: Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    height: 100,
                    child: TextFormField(
                      maxLines: 99,
                      controller: captionController,
                      decoration: new InputDecoration(
                        contentPadding:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        hintText: "Write a caption ...",
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      style: new TextStyle(
                          fontFamily: "Poppins", color: Color(colorText)),
                    )),
              )
            ],
          ),
          Expanded(
              child: Container(
            margin: EdgeInsets.only(bottom: 20, top: 20),
            alignment: Alignment.topCenter,
            child: RaisedButton(
                color: Color(colorPrimary),
                child: Text(
                  "GRAM",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  if (UserImage.imageFile != null) {
                    APIServices.addPost(
                        context, captionController.text, UserImage.fileList);
                  } else if (UserImage.imageFile == null) {
                    Utils().errorDialog(
                        context, "UPLOAD", "please select file(s) to upload");
                  }
                },
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0))),
          ))
        ],
      ),
    );
  }
}
