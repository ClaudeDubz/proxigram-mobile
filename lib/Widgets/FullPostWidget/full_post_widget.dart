import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:proxigram/Global/constants.dart';
import 'package:proxigram/Global/utils.dart';
import 'package:proxigram/Models/V2/comment.dart';
import 'package:proxigram/Models/V2/content.dart';
import 'package:proxigram/Models/V2/user.dart';
import 'package:proxigram/Services/api_services.dart';
import 'package:proxigram/Theme/colors.dart';
import 'package:proxigram/Widgets/ChewieItemWidget/feed_video_item_widget.dart';
import 'package:proxigram/Widgets/GradientOutlineWidget/gradient_outline_widget.dart';
import 'package:proxigram/Widgets/PostWidget/Animation/star_icon_animator.dart';
import 'package:proxigram/Widgets/PostWidget/Animation/star_overlay_animator.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/add_comment_modal.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_comment_modal.dart';
import 'package:proxigram/Widgets/PostWidget/Modals/view_post_modal.dart';
import 'package:proxigram/Widgets/PostWidget/post_description_widget.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';
import 'package:path_provider/path_provider.dart';

class FullPostWidget extends StatefulWidget {
  FullPostWidget(
      {@required this.userImage,
      @required this.firstName,
      @required this.lastName,
      @required this.imageType,
      @required this.isPostLiked,
      @required this.postID,
      @required this.likesCount,
      @required this.commentList,
      @required this.createdAt,
      @required this.caption,
      @required this.content});

  final String userImage;
  final String firstName;
  final String lastName;
  final String imageType;
  final int isPostLiked;
  final int postID;
  int likesCount;
  final List<Comment> commentList;
  final DateTime createdAt;
  final String caption;
  final List<Contents> content;

  @override
  _FullPostWidgetState createState() => _FullPostWidgetState();
}

class _FullPostWidgetState extends State<FullPostWidget>
    with TickerProviderStateMixin {
  bool isPostLiked = false;
  String thumbnail;
  final StreamController<void> _doubleTapImageEvents =
      StreamController.broadcast();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.isPostLiked == 1) {
      isPostLiked = true;
    } else {
      isPostLiked = false;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _doubleTapImageEvents.close();
    super.dispose();
  }

  void _onDoubleTapLikePhoto() {
    _doubleTapImageEvents.sink.add(null);

    setState(() {
      isPostLiked = !isPostLiked;
      if (isPostLiked) {
        setState(() {
          widget.likesCount++;
        });
      } else {
        setState(() {
          widget.likesCount--;
        });
      }
    });

    APIServices.likePost(widget.postID, context);
  }

  void toggleLikePost() {
    _doubleTapImageEvents.sink.add(null);

    setState(() {
      isPostLiked = !isPostLiked;
      if (isPostLiked) {
        setState(() {
          widget.likesCount++;
        });
      } else {
        setState(() {
          widget.likesCount++;
        });
      }
    });

    APIServices.likePost(widget.postID, context);
  }

  Future getVideoThumbnail(String videoPathUrl) async {
    thumbnail = await VideoThumbnail.thumbnailFile(
      video: videoPathUrl,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.PNG, // the original resolution of the video
      quality: 75,
    );

    return File(thumbnail);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(top: 20, left: 10, right: 10),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.angleLeft,
                        color: Color(colorPrimary),
                      ),
                      Text(
                        "Back",
                        style: TextStyle(
                          color: Color(colorPrimary),
                          fontSize: 18,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  )),
              userInfo(widget.userImage, widget.firstName, widget.lastName),
              userPost(widget.content),
              postIcons(
                widget.postID,
                widget.commentList,
                widget.createdAt,
              ),
              postLikesCount(widget.likesCount),
              postDescription(
                  widget.caption, widget.firstName, widget.lastName),
              postComment(widget.commentList, widget.postID),
              addComment(widget.postID, widget.commentList),
            ],
          ),
        ),
      ),
    );
  }

  Widget userInfo(String image, String firstName, String lastName) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(top: 20),
            width: 60,
            height: 60,
            child: GradientStroke(
              strokeWidth: 2,
              radius: 50,
              gradient: LinearGradient(
                  colors: [Color(colorPrimary), Color(colorSecondary)]),
              child: Container(
                height: 50,
                width: 50,
                child: ClipRRect(
                    borderRadius: new BorderRadius.circular(50),
                    child: image == null
                        ? Container(
                            color: Color(colorPrimary),
                            alignment: Alignment.center,
                            child: Text(
                              firstName[0].toUpperCase(),
                              style: TextStyle(
                                  color: Color(colorText), fontSize: 20),
                            ),
                          )
                        : CachedNetworkImage(
                            imageUrl: "${APIServices.httpDomain}$image",
                            placeholder: (context, url) =>
                                Image.asset('assets/app/app_icon.png'),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                            fit: BoxFit.cover,
                          )),
              ),
              onPressed: () {},
            )),
        Expanded(
            child: Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 10),
                      child: Text(
                        firstName + " " + lastName,
                        style: TextStyle(
                            fontSize: 18,
                            color: Color(colorTertiary),
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ))),
      ],
    );
  }

  Widget userPost(List<Contents> content) {
    return Container(
        padding: EdgeInsets.only(top: 10, bottom: 5),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.width,
        child: GestureDetector(
            onDoubleTap: () {
              _onDoubleTapLikePhoto();
            },
            onLongPress: () {
              // showImageModal(
              //     "${APIServices.httpDomain}$image", "$imageType", index);
            },
            child: Stack(
              children: <Widget>[
                Container(
                    height: double.infinity,
                    width: MediaQuery.of(context).size.width,
                    child: ClipRRect(
                        borderRadius: new BorderRadius.circular(0),
                        child: content.length == 1
                            ? getImageType(content[0].content) == "image"
                                ? CachedNetworkImage(
                                    imageUrl:
                                        "${APIServices.httpDomain}${content[0].content}",
                                    placeholder: (context, url) =>
                                        Image.asset('assets/app/app_icon.png'),
                                    errorWidget: (context, url, error) =>
                                        new Icon(Icons.error),
                                    fit: BoxFit.cover,
                                  )
                                : ChewieFeedItem(
                                    videoPlayerController:
                                        VideoPlayerController.network(
                                      "${APIServices.httpDomain}${content[0].content}",
                                    ),
                                    looping: true,
                                    aspectRatio: VideoPlayerController.network(
                                      "${APIServices.httpDomain}${content[0].content}",
                                    ).value.aspectRatio)
                            : Swiper(
                                itemBuilder: (BuildContext context, int i) {
                                  return Container(
                                      padding:
                                          EdgeInsets.only(right: 5, left: 5),
                                      child: ClipRRect(
                                          borderRadius:
                                              new BorderRadius.circular(0),
                                          child: getImageType(
                                                      content[i].content) ==
                                                  "image"
                                              ? CachedNetworkImage(
                                                  imageUrl:
                                                      "${APIServices.httpDomain}${content[i].content}",
                                                  placeholder: (context, url) =>
                                                      Image.asset(
                                                          'assets/app/app_icon.png'),
                                                  errorWidget:
                                                      (context, url, error) =>
                                                          new Icon(Icons.error),
                                                  fit: BoxFit.cover,
                                                )
                                              : ChewieFeedItem(
                                                  videoPlayerController:
                                                      VideoPlayerController
                                                          .network(
                                                    "${APIServices.httpDomain}${content[i].content}",
                                                  ),
                                                  looping: true,
                                                  aspectRatio:
                                                      VideoPlayerController
                                                          .network(
                                                    "${APIServices.httpDomain}${content[i].content}",
                                                  ).value.aspectRatio)));
                                },
                                itemCount: content.length,
                                pagination: new SwiperPagination(
                                    builder: DotSwiperPaginationBuilder(
                                        color: Colors.white.withOpacity(0.75),
                                        activeColor:
                                            Theme.of(context).primaryColor)),
                                loop: false,
                                duration: 100,
                              ))),
                Center(
                    child: StarOverlayAnimator(
                        triggerAnimationStream: _doubleTapImageEvents.stream))
              ],
            )));
  }

  Widget postIcons(int postID, List<Comment> commentList, DateTime createdAt) {
    return Container(
        margin: EdgeInsets.only(left: 10),
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(right: 15),
                alignment: Alignment.center,
                child: StarIconAnimator(
                  isLiked: isPostLiked,
                  size: 30.0,
                  onTap: () {
                    toggleLikePost();
                  },
                  triggerAnimationStream: _doubleTapImageEvents.stream,
                )),
            Container(
              margin: EdgeInsets.only(right: 15),
              alignment: Alignment.center,
              child: GestureDetector(
                child: SvgPicture.asset(
                  "assets/icons/comment_icon_stroke.svg",
                  color: Color(colorSecondary),
                  height: 30,
                  width: 30,
                ),
                onTap: () {
                  showCommentsModal(postID, commentList);
                },
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: GestureDetector(
                child: SvgPicture.asset(
                  "assets/icons/share_icon_stroke.svg",
                  color: Color(colorSecondary),
                  height: 30,
                  width: 30,
                ),
                onTap: () {},
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                  alignment: Alignment.topCenter,
                  margin: EdgeInsets.only(top: 5, left: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text("Posted "),
                      Utils().getNotificationDate(createdAt)
                    ],
                  )),
            ),
          ],
        ));
  }

  Widget addComment(int id, List<Comment> commentsList) {
    return Container(
        margin: EdgeInsets.all(15),
        width: MediaQuery.of(context).size.width,
        child: Row(
          children: <Widget>[
            Container(
                height: 25,
                width: 25,
                child: ClipRRect(
                    borderRadius: new BorderRadius.circular(50),
                    child: Constants.userProfilePhoto == null
                        ? Container(
                            color: Color(colorPrimary),
                            alignment: Alignment.center,
                            child: Text(
                              Constants.userFirstName[0].toUpperCase(),
                              style: TextStyle(
                                  color: Color(colorText), fontSize: 20),
                            ),
                          )
                        : CachedNetworkImage(
                            imageUrl:
                                "${APIServices.httpDomain}${Constants.userProfilePhoto}",
                            placeholder: (context, url) =>
                                Image.asset('assets/app/app_icon.png'),
                            errorWidget: (context, url, error) =>
                                new Icon(Icons.error),
                            fit: BoxFit.cover,
                          ))),
            Container(
                margin: EdgeInsets.only(left: 10),
                child: InkWell(
                    onTap: () => addCommentModal(id, commentsList),
                    child: Text(
                      "Add a comment ...",
                      style: TextStyle(color: Color(colorText)),
                    )))
          ],
        ));
  }

  Widget postComment(List<Comment> comments, int id) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(left: 15, right: 15, top: 5),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: GestureDetector(
                onTap: () {},
                child: comments.length != 0
                    ? comments.length == 1
                        ? InkWell(
                            onTap: () {
                              showCommentsModal(id, comments);
                            },
                            child: Text(
                              "View ${widget.commentList.length} comment",
                              style: TextStyle(
                                  fontSize: 15, color: Color(colorText)),
                            ))
                        : InkWell(
                            onTap: () {
                              showCommentsModal(id, comments);
                            },
                            child: Text(
                              "View all ${widget.commentList.length} comments",
                              style: TextStyle(
                                  fontSize: 15, color: Color(colorText)),
                            ))
                    : Text(
                        "Be the first to comment!",
                        style: TextStyle(fontSize: 15, color: Color(colorText)),
                      )),
          ),
        ],
      ),
    );
  }

  Widget postDescription(String description, String fname, String lname) {
    return Container(
        margin: EdgeInsets.only(
          left: 15,
          top: 10,
        ),
        child: ExpandableDescription(
            description: description, user: "@$fname$lname"));
  }

  Widget postLikesCount(int likesCount) {
    return Container(
      margin: EdgeInsets.only(left: 15),
      child: Row(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10),
            child: Text(
              widget.likesCount.toString(),
              style: TextStyle(
                  color: Color(colorTertiary),
                  fontSize: 15,
                  fontWeight: FontWeight.w700),
            ),
          ),
          Container(
              child: Text(
            widget.likesCount == 1 ? "Like" : "Likes",
            style: TextStyle(
                color: Color(colorTertiary),
                fontSize: 15,
                fontWeight: FontWeight.w500),
          ))
        ],
      ),
    );
  }

  void showImageModal(String imageURL, String imageType) {
    if (imageType == "image") {
      showModalBottomSheet(
        backgroundColor: Color(colorBackground),
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: ViewPostModal(
              imageProvider: NetworkImage(imageURL),
            ),
          );
        },
      );
    } else {
      showModalBottomSheet(
        backgroundColor: Color(colorBackground),
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
              color: Colors.black,
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
                Container(
                    margin: EdgeInsets.only(left: 20, top: 30),
                    child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.angleLeft,
                              color: Color(colorBackground),
                            ),
                            Text(
                              "Back",
                              style: TextStyle(
                                color: Color(colorBackground),
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ))),
                Expanded(
                    child: imageType == "video"
                        ? ChewieFeedItem(
                            videoPlayerController:
                                VideoPlayerController.network(
                              "${APIServices.httpDomain}$imageURL",
                            ),
                            looping: true,
                            aspectRatio: VideoPlayerController.network(
                              "${APIServices.httpDomain}$imageURL",
                            ).value.aspectRatio)
                        : Container())
              ]));
        },
      );
    }
  }

  void showCommentsModal(int id, List<Comment> comments) {
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      backgroundColor: Color(colorBackground),
      isScrollControlled: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Color(colorBackground),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20))),
          height: MediaQuery.of(context).size.height / 1.5,
          width: MediaQuery.of(context).size.width,
          child: ViewCommentModal(
            postID: id,
            comments: comments,
          ),
        );
      },
    );
  }

  void addCommentModal(int i, List<Comment> commentsList) {
    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Container(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: AddCommentModal(
            onPost: (String text) {
              APIServices.commentPost(context, i.toString(), text);
              commentsList.add(Comment(
                  id: 1,
                  comment: text,
                  createdAt: DateTime.now().toString(),
                  deletedAt: "",
                  isLiked: 0,
                  likes: null,
                  likesCount: 0,
                  postId: 1,
                  updatedAt: "",
                  user: User(
                      image: Constants.userProfilePhoto,
                      fname: Constants.userFirstName,
                      lname: Constants.userLastName),
                  userId: Constants.userID));

              Navigator.pop(context);
            },
          ),
        );
      },
    );
  }

  getImageType(String path) {
    if (path.toLowerCase().substring(path.length - 3) == "jpg" ||
        path.toLowerCase().substring(path.length - 3) == "peg" ||
        path.toLowerCase().substring(path.length - 3) == "gif" ||
        path.toLowerCase().substring(path.length - 3) == "png") {
      return "image";
    } else {
      return "video";
    }
  }
}
